import { light as lightTheme, mapping } from '@eva-design/eva';
import { EvaIconsPack } from '@ui-kitten/eva-icons';
import React from 'react';
import { ApplicationProvider, IconRegistry } from 'react-native-ui-kitten';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { AppContainer } from "./src/navigation";
import { persistor, store } from "./src/reducers";
import OneSignal from 'react-native-onesignal';

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

React.Component.prototype._setStateAsync = function (state) {
  return new Promise((resolve) => { this.setState(state, resolve) });
}

React.PureComponent.prototype._setStateAsync = function (state) {
  return new Promise((resolve) => { this.setState(state, resolve) });
}

class App extends React.Component {
  constructor(props) {
    super(props);

    OneSignal.init("160062cf-5ed7-4b22-b702-753309611c9e");

    OneSignal.addEventListener('received', this.onReceived);
    OneSignal.addEventListener('opened', this.onOpened);
    OneSignal.addEventListener('ids', this.onIds); OneSignal.init("160062cf-5ed7-4b22-b702-753309611c9e");

    OneSignal.addEventListener('received', this.onReceived);
    OneSignal.addEventListener('opened', this.onOpened);
    OneSignal.addEventListener('ids', this.onIds);
  }

  onReceived(notification) {
    console.log("Notification received: ", notification);
  }

  onOpened(openResult) {
    console.log('Message: ', openResult.notification.payload.body);
    console.log('Data: ', openResult.notification.payload.additionalData);
    console.log('isActive: ', openResult.notification.isAppInFocus);
    console.log('openResult: ', openResult);
  }

  onIds(device) {
    console.log('Device info: ', device);
  }

  render() {
    return (
      <>
        <IconRegistry icons={EvaIconsPack} />
        <ApplicationProvider
          mapping={mapping}
          theme={lightTheme}
        >
          <Provider store={store}>
            <PersistGate loading={null} persistor={persistor}>
              <AppContainer />
            </PersistGate>
          </Provider>
        </ApplicationProvider>
      </>
    )
  }
}

export default App;
