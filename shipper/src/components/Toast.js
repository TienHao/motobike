// libary
import React, { PureComponent } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Icon } from 'react-native-ui-kitten';
import { getHeightPercentage, getWidthPercentage } from 'utils';

const widthToast = 150;

const toastType = {
  success: "rgba(76,175,80,1)",
  error: "rgba(244,67,54,1)",
  info: "rgba(33,150,243,1)",
  warning: "rgba(255,255,59,1)"
}

const styles = StyleSheet.create({
  container: {
    zIndex: 100,
    position: 'absolute',
    top: (getHeightPercentage(100) / 2) - (widthToast),
    left: (getWidthPercentage(100) / 2) - (widthToast / 2),
    justifyContent: "center",
    alignItems: "center",
    width: widthToast,
    height: widthToast,
    borderRadius: 8
  },
})

class Toast extends PureComponent {
  constructor(props) {
    super(props);

    this.durationEnd = false;
    this.duration = 1000;
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.active) {
      this.durationEnd = false;
      this.forceUpdate()
      this._timeStart()
    }
  }

  _timeStart = () => {
    this.start = Date.now()
    this.time = setInterval(() => {
      if (Date.now() - this.start > this.duration && !this.durationEnd) {
        this._timeEnd()
        clearInterval(this.time)
      }
    }, 1000);
  }

  _timeEnd = () => {
    this.durationEnd = true
    this.forceUpdate()
  }

  render() {
    const { type } = this.props

    return (
      (this.props.active && !this.durationEnd) &&
      <View style={[styles.container, { backgroundColor: toastType[type] || "rgba(0,0,0,0.5)" }]}>
        <Icon
          name={type === "success" ? "checkmark-outline" : type === "error" ? "close-outline" : "hash-outline"}
          width={72}
          height={72}
          fill={"#fff"}
        />
        <Text style={{ textAlign: "center", color: "#fff" }}>
          {this.props.message}
        </Text>
      </View> || null
    )
  }
}

export { Toast };

