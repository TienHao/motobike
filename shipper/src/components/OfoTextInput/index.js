// libary
import React, { Component } from 'react';
import { View, Text, TextInput } from 'react-native';

// styles
import { styles } from './styles';

class OfoTextInput extends Component {
    constructor(props) {
        super(props);
    }

    onChangeText = (value) => {
        const { name } = this.props;
        this.props.onChangeText(name, value);
    }

    onEndEditingCheckError = (value) => {
        const { name } = this.props;
        this.props.onEndEditingCheckError(name, value);
    }

    render() {
        const { name, value, type } = this.props;
        return (
            <React.Fragment>
                <View style={[styles.container, this.props.styles]}>
                    <Text
                        style={[
                            styles.label,
                            {
                                color: this.props.error ? "red" : this.props.success ? "green" : "rgba(0,0,0,0.6)"
                            }
                        ]}
                    >
                        {this.props.label}
                    </Text>
                    <TextInput
                        placeholder={this.props.placeholder || "Input"}
                        onChangeText={(text) => this.onChangeText(text)}
                        name={name}
                        multiline={this.props.multiline}
                        value={value}
                        secureTextEntry={type === 'password'}
                        onEndEditing={(e) => this.onEndEditingCheckError(e.nativeEvent.text)}
                        style={[
                            styles.placeHolder,
                            {
                                borderBottomColor: this.props.error ? "red" : this.props.success ? "green" : "#D9D5DC"
                            }
                        ]}
                    />
                    {this.props.error ? (
                        <Text
                            style={[
                                styles.errorMessage,
                                {
                                    color: this.props.error ? "red" : "transparent"
                                }
                            ]}
                        >
                            {this.props.errorMessage || "Bắt buộc nhập"}
                        </Text>
                    ) : null}
                </View>
            </React.Fragment>
        );
    }
}

export default OfoTextInput;