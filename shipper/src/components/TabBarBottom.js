import React from 'react';
import { Keyboard } from 'react-native';
import { BottomNavigation, BottomNavigationTab } from 'react-native-ui-kitten';
import { colors } from '../config/colors';

class TabBarBottom extends React.PureComponent {
  constructor(props) {
    super(props)

    this.keyboardWillShow = this.keyboardWillShow.bind(this);
    this.keyboardWillHide = this.keyboardWillHide.bind(this);

    this.state = {
      isVisible: true
    }
  }

  UNSAFE_componentWillMount() {
    this.keyboardWillShowSub = Keyboard.addListener('keyboardDidShow', this.keyboardWillShow);
    this.keyboardWillHideSub = Keyboard.addListener('keyboardDidHide', this.keyboardWillHide);
  }

  componentWillUnmount() {
    this.keyboardWillShowSub.remove();
    this.keyboardWillHideSub.remove();
  }

  keyboardWillShow = event => {
    this.setState({
      isVisible: false
    })
  }

  keyboardWillHide = event => {
    this.setState({
      isVisible: true
    })
  }

  render() {
    const {
      navigation: { state: { index, routes } },
      style,
      activeTintColor,
      inactiveTintColor,
      renderIcon,
      getLabelText,
      showLabel,
      onTabSelect
    } = this.props;

    return this.state.isVisible ?
      <BottomNavigation
        style={{
          ...style,
        }}
        selectedIndex={index}
        onSelect={onTabSelect}
        indicatorStyle={{ backgroundColor: colors.yellow.default, height: 2 }}
      >
        {
          routes.map((route, idx) => {
            return (
              <BottomNavigationTab
                {...this.props}
                titleStyle={{
                  color: index === idx ? activeTintColor : inactiveTintColor
                }}
                key={idx}
                title={showLabel && getLabelText({ route })}
                icon={() => renderIcon({
                  route,
                  focused: index === idx,
                  tintColor: index === idx ? activeTintColor : inactiveTintColor
                })}
              />
            )
          })
        }
      </BottomNavigation>
      :
      null
  }
}

export { TabBarBottom };

