// libary
import React, { PureComponent } from 'react';
import { StyleSheet, TextInput, TouchableOpacity, View, Text, StatusBar } from 'react-native';
import { Icon } from 'react-native-eva-icons';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

// constants
import {
  SHIPPER,
  CUSTOMMER,
  RENDER_PAGE_CANCEL_ORDER,
  RENDER_PAGE_COMPLETE_ORDER,
  RENDER_PAGE_IN_PROGRESS_ORDER
} from '../constanst';

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#1F2D6A',
    paddingTop: getStatusBarHeight() + 10,
    paddingLeft: 10,
    paddingRight: 10,
    height: 120,
    shadowColor: "black",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },

  containerInputIcon: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: 'white',
    borderRadius: 5,
    height: 40,
  },
  pageActive: {
    borderColor: '#FFA910',
    borderBottomWidth: 2
  }
})

class HeaderContainer extends PureComponent {
  constructor(props) {
    super(props)

    this.state = this._getInitialState();
  }

  _getInitialState = () => {
    return {
      currentValueSearch: "",
      activeStyles: RENDER_PAGE_COMPLETE_ORDER
    }
  }

  _search = () => {
    this.props.navigation.navigate('HistoryTree', { history: this.state.currentValueSearch });
  }

  onPressRenderPageCancelOrder = () => {
    this.setState({
      activeStyles: RENDER_PAGE_CANCEL_ORDER
    })

    this.props.callbackCheckPageRender(RENDER_PAGE_CANCEL_ORDER);
  }

  onPressRenderPageOrderInProgress = () => {
    this.setState({
      activeStyles: RENDER_PAGE_IN_PROGRESS_ORDER
    })

    this.props.callbackCheckPageRender(RENDER_PAGE_IN_PROGRESS_ORDER);
  }

  onPressRenderPageOrderComplete = () => {
    this.setState({
      activeStyles: RENDER_PAGE_COMPLETE_ORDER
    });

    this.props.callbackCheckPageRender(RENDER_PAGE_COMPLETE_ORDER);
  }

  render() {
    const { currentValueSearch } = this.state;
    const { userInfo } = this.props;

    return (
      <React.Fragment>
        <StatusBar translucent backgroundColor={'grey'} barStyle="light-content">
        </StatusBar>
        <View style={styles.container}>
          {userInfo.type_id == SHIPPER ?
            (<View>
              <Text style={{ fontSize: 20, fontWeight: 'bold', color: 'white' }}>Đơn hàng</Text>
            </View>) : userInfo.type_id == CUSTOMMER ? (
              <View style={styles.containerInputIcon}>
                <View style={{ flex: 1 }}>
                  <TextInput
                    style={{ paddingLeft: 8 }}
                    placeholder={"Tra cứu đơn hàng"}
                    value={currentValueSearch}
                    onChangeText={(text) => this.setState({ currentValueSearch: text })}
                    onSubmitEditing={() => this._search()}
                  />
                </View>
                <View style={{ paddingRight: 8 }}>
                  <TouchableOpacity onPress={() => this._search()}>
                    <Icon name='search-outline' width={24} height={24} fill={'gray'} />
                  </TouchableOpacity>
                </View>
              </View>
            ) : (<View>
              <Text style={{ fontSize: 20, fontWeight: 'bold', color: 'white' }}>Đơn hàng</Text>
            </View>)}
          <View style={{ flex: 1, flexDirection: 'row' }}>
            {userInfo.type_id == SHIPPER ? (
              null
            ) : (
                <TouchableOpacity style={[{ flex: 3, justifyContent: 'center', alignItems: 'center' }, this.state.activeStyles === RENDER_PAGE_IN_PROGRESS_ORDER ? styles.pageActive : null]} onPress={this.onPressRenderPageOrderInProgress}>
                  <Text style={this.state.activeStyles === RENDER_PAGE_IN_PROGRESS_ORDER ? { color: '#FFA910' } : { color: 'white' }}>{userInfo.type_id == SHIPPER ? "Hoàn Thành" : "Đang hoạt động"}</Text>
                </TouchableOpacity>
              )}
            <TouchableOpacity style={[{ flex: 3, justifyContent: 'center', alignItems: 'center' }, this.state.activeStyles === RENDER_PAGE_COMPLETE_ORDER ? styles.pageActive : null]} onPress={this.onPressRenderPageOrderComplete}>
              <Text style={this.state.activeStyles === RENDER_PAGE_COMPLETE_ORDER ? { color: '#FFA910' } : { color: 'white' }}>{userInfo.type_id == SHIPPER ? "Đơn hàng đã chọn" : "Hoàn thành"}</Text>
            </TouchableOpacity>
            {userInfo.type_id == SHIPPER ? (
              <TouchableOpacity style={[{ flex: 3, justifyContent: 'center', alignItems: 'center' }, this.state.activeStyles === RENDER_PAGE_IN_PROGRESS_ORDER ? styles.pageActive : null]} onPress={this.onPressRenderPageOrderInProgress}>
                <Text style={this.state.activeStyles === RENDER_PAGE_IN_PROGRESS_ORDER ? { color: '#FFA910' } : { color: 'white' }}>{userInfo.type_id == SHIPPER ? "Đơn hàng mới" : "Đang hoạt động"}</Text>
              </TouchableOpacity>
            ) : (
                <TouchableOpacity style={[{ flex: 3, justifyContent: 'center', alignItems: 'center' }, this.state.activeStyles === RENDER_PAGE_CANCEL_ORDER ? styles.pageActive : null]} onPress={this.onPressRenderPageCancelOrder}>
                  <Text style={this.state.activeStyles === RENDER_PAGE_CANCEL_ORDER ? { color: '#FFA910' } : { color: 'white' }}>{userInfo.type_id == SHIPPER ? "Đơn hàng" : "Đã hủy"}</Text>
                </TouchableOpacity>
              )}
          </View>
        </View>
      </React.Fragment>
    )
  }
}

function mapStateToProps(state) {
  return {
    userInfo: state.authReducer.userInfo
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch)
}

const HeaderComp = connect(mapStateToProps, mapDispatchToProps)(HeaderContainer);
export { HeaderComp };