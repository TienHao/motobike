import { colors } from 'config/colors';
import React, { Component, Fragment } from 'react';
import { ActivityIndicator, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Icon } from 'react-native-eva-icons';
import Geolocation from 'react-native-geolocation-service';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { permissionType, takePermission } from 'utils';
const GOOGLE_MAPS_APIKEY = "AIzaSyCg21kDM0otK2dkU3jCo3ACpnBMJXJVLho";

const styles = StyleSheet.create({
  containerMap: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
    flex: 1
  },
  container: {
    justifyContent: "center",
    alignItems: "center",
    flex: 1
  },

  containerControl: {
    position: 'absolute',
    bottom: 0,
    alignSelf: 'center',
    flex: 1
  },

  containerBack: {
    position: 'absolute',
    top: 50,
    left: 20,
    backgroundColor: "rgba(255, 255, 255, 0.5)",
    padding: 16,
    borderRadius: 16
  },

  btn: {
    padding: 16,
    borderRadius: 8,
    backgroundColor: "#fff",
    textAlign: "center",
  },
})

class GoogleMapContainer extends Component {
  static navigationOptions = {
    headerTransparent: true
  };

  constructor(props) {
    super(props)
    this.state = this._getInitialState()
  }

  _getInitialState = () => {
    return {
      permissionMessage: "",
      permissionLocation: false,
      isGetCurrentLocation: false,
      isInitialRegion: true,
      isDraw: false,
      currentRegion: null,
      destinationMarker: {
        latitude: undefined,
        longitude: undefined,
        latitudeDelta: 0.02,
        longitudeDelta: 0.02,
      },

      currentLocation: {
        latitude: 0,
        longitude: 0,
        latitudeDelta: 0.02,
        longitudeDelta: 0.02,
      },
      region: {
        latitude: 10.9529564,
        longitude: 106.8025003,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
      },
    };
  }

  async componentDidMount() {
    const accept = await takePermission(permissionType.location)
    if (accept) {
      this.setState({ permissionLocation: true })
      this._getCurrentLocation()
    } else {
      this.setState({ permissionMessage: "Bạn cần phải cho phép truy cập vị trí để sử dụng chức năng này" })
    }
  }

  _getCurrentLocation = () => {
    Geolocation.getCurrentPosition(
      (position) => {
        const { coords } = position
        this.setState({
          currentLocation: {
            ...this.state.currentLocation,
            latitude: coords.latitude,
            longitude: coords.longitude
          },
          isGetCurrentLocation: true
        })
      },
      (error) => {
        this.setState({ permissionLocation: false })
        this.setState({ permissionMessage: "Request time out!" })
        console.log(error.code, error.message);
      },
      { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
    );
  }

  _onRegionChange = (region) => {
    this.setState({ currentRegion: region });
  }

  _onLongPressMapView = ({ coordinate, position }) => {
    const { isDraw, currentRegion } = this.state
    const destinationMarker = {
      ...coordinate,
      latitudeDelta: currentRegion.latitudeDelta,
      longitudeDelta: currentRegion.latitudeDelta
    }
    this.setState({ destinationMarker, isInitialRegion: false })
    if (isDraw) this.setState({ isDraw: false })
  }

  _renderDestinationMarker = () => {
    const { destinationMarker, isDraw } = this.state
    if (!destinationMarker.latitude || !destinationMarker.longitude) return null

    return (
      <MapView.Marker
        coordinate={{
          latitude: destinationMarker.latitude,
          longitude: destinationMarker.longitude
        }}
        title={"Điểm tới"}
        description={"mô tả Điểm tới"}
      />
    )
  }

  _drawDirection = () => {
    this.setState({ isDraw: true })
  }

  _renderDrawStroke = () => {
    const { currentLocation, destinationMarker, isDraw } = this.state
    if (!isDraw) return null

    return (
      <MapViewDirections
        apikey={GOOGLE_MAPS_APIKEY}
        strokeWidth={3}
        strokeColor="red"
        origin={{
          longitude: currentLocation.longitude,
          latitude: currentLocation.latitude
        }}
        destination={{
          latitude: destinationMarker.latitude,
          longitude: destinationMarker.longitude
        }}
      />
    )
  }

  render() {
    let {
      isGetCurrentLocation,
      currentLocation,
      isInitialRegion,
      permissionMessage
    } = this.state

    return (
      <View style={styles.containerMap}>
        {
          isGetCurrentLocation
          &&
          <Fragment>
            <MapView
              provider={PROVIDER_GOOGLE}
              style={styles.map}
              showsMyLocationButton={true}
              showsUserLocation={true}
              followsUserLocation={true}
              initialRegion={isInitialRegion ? currentLocation : undefined}
              onLongPress={event => this._onLongPressMapView(event.nativeEvent)}
              onRegionChange={this._onRegionChange}
            >
              <MapView.Marker
                coordinate={{
                  latitude: currentLocation.latitude,
                  longitude: currentLocation.longitude
                }}
                title={"Vị trí hiện tại"}
                description={"mô tả"}
              />
              {this._renderDestinationMarker()}
              {this._renderDrawStroke()}
            </MapView>
            <View style={styles.containerBack}>
              <TouchableOpacity
                onPress={() => this.props.navigation.pop()}
              >
                <Text>
                  <Icon name='arrow-ios-back-outline' width={32} height={32} fill={'#000'} />
                </Text>
              </TouchableOpacity>
            </View>
            <View
              style={styles.containerControl}
            >
              <TouchableOpacity
                style={styles.btn}
                onPress={this._drawDirection}
              >
                <Text>Vẽ đường (Test)</Text>
              </TouchableOpacity>
            </View>
          </Fragment>
          ||
          <View
            style={styles.container}
          >
            {
              !permissionMessage
              && <ActivityIndicator size="large" color={colors.primary.default} />
              ||
              <View style={{ justifyContent: "center", alignItems: "center", paddingHorizontal: 16 }}>
                <Text style={{ textAlign: "center" }}>{permissionMessage}</Text>
              </View>
            }

          </View>
        }
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch)
}

const GoogleMapComp = connect(mapStateToProps, mapDispatchToProps)(GoogleMapContainer);
export { GoogleMapComp };
