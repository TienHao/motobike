import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    radioButton: {
        flexDirection: 'row',
        margin: 0,
        alignItems: 'center',
        justifyContent: 'center'
    },
    radioButtonGroup: {
        borderRadius: 50,
        borderWidth: 2,
        justifyContent: 'center',
        alignItems: 'center'
    },
    radioIcon: {
        borderRadius: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },
    label: {
        marginLeft: 10,
        fontSize: 20
    }
});