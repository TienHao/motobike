// libary
import React, { Component } from 'react';
import { TextInput, View, StyleSheet, Text, Keyboard, TouchableHighlight } from 'react-native';
import Geolocation from 'react-native-geolocation-service';
import _ from 'lodash';

const apiKey = "AIzaSyCx3c8GVP57CDXvA8l1iCNRlMFrXdj7G_0";

class SearchLocations extends Component {
    constructor(props) {
        super(props);

        this.state = {
            error: "",
            latitude: 0,
            longitude: 0,
            destination: "",
            locationPredictions: []
        };
        this.onChangeTextGetLocationDebounced = _.debounce(this.onChangeTextGetLocation, 1000);

    }

    getCurrentLocation = () => {
        Geolocation.getCurrentPosition(
            (position) => {
                const { coords } = position;
                this.setState({
                    currentLocation: {
                        latitude: coords.latitude,
                        longitude: coords.longitude
                    }
                })
            },
            (error) => {
                this.setState({ error: error });
            },
            { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
        );
    }

    componentDidMount() {
        this.getCurrentLocation();
    }

    onChangeTextGetLocation = async (location) => {
        this.setState({
            destination: location
        });

        const apiUrl = `https://maps.googleapis.com/maps/api/place/autocomplete/json?key=${apiKey}&input=${location}&location=${this.state.latitude}, ${this.state.longitude}&radius=2000`;

        try {
            const result = await fetch(apiUrl);
            const json = await result.json();

            this.setState({
                locationPredictions: json.predictions
            });

        } catch (error) {
            console.error(error);
        }
    }

    onpressedPrediction(prediction) {
        Keyboard.dismiss();

        this.setState({
            locationPrediction: [],
            destination: prediction.description
        });
        Keyboard;
    }

    render() {
        const predictions = this.state.locationPredictions.map(prediction => (
            <TouchableHighlight
                key={prediction.id}
                onPress={() => this.onpressedPrediction(prediction)}
            >
                <Text style={styles.prediction}>{prediction.description}</Text>
            </TouchableHighlight>
        ))

        return (
            <View>
                <TextInput
                    placeholder="Nhập vào địa chỉ*"
                    style={styles.inputSearchLocation}
                    value={this.state.destination}
                    onChangeText={(text) => this.onChangeTextGetLocation(text)}
                />
                {predictions}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    inputSearchLocation: {
        height: 40,
        borderWidth: 1,
        marginTop: 50,
        marginLeft: 5,
        marginRight: 5
    },
    prediction: {
        backgroundColor: '#fff',
        padding: 5,
        fontSize: 18,
        borderWidth: 0.5,
        marginLeft: 5,
        marginRight: 5
    }
});

export default SearchLocations;