// constants
import {
    GET_CHECK_PEOPLE_ADDRESS_DELIVERY,
    GET_ADDRESS_DELIVERY
} from '../constanst';

const initState = {
    addressDeliver: '',
    addressReceiver: '',
    valueCheckPeople: 0,
    location: {}
}

export function mapReducer(state = initState, action) {
    let newState = { ...state };

    if (action.type === GET_CHECK_PEOPLE_ADDRESS_DELIVERY) {

        newState = {
            ...state,
            valueCheckPeople: action.payload.valueCheckPeople
        }
        return newState;

    } else if (action.type === GET_ADDRESS_DELIVERY) {

        newState = {
            ...state,
            addressDeliver: action.payload.addressDeliver === undefined ? state.addressDeliver : action.payload.addressDeliver,
            addressReceiver: action.payload.addressReceiver === undefined ? state.addressReceiver : action.payload.addressReceiver,
            location: action.payload.location
        };

        return newState;
    } else {
        return newState;
    }
}