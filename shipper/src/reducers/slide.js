import * as actions from 'actions/index';

const defaultState = {
  isLoading: false,
  error: "",
  data: ""
}

export function slideReducer(state = defaultState, action) {
  let nextState = {...state}
  const { payload, type} = action

  switch(type) {
    case actions.GET_SLIDE: {
      nextState.isLoading = true

      return nextState
    }


    case actions.GET_SLIDE_SUCCESS: {
      nextState.isLoading = false
      nextState.data = payload.data

      return nextState
    }


    case actions.GET_SLIDE_FAIL: {
      nextState.isLoading = false
      nextState.error = payload.error

      return nextState
    }
    default: {
      return state
    }

  }
}
