// constants
import {
    GET_DATE_TIME,
    GET_FEE,
    GET_MONEY_FEE
} from '../constanst';

const initState = {
    dateTime: "",
    checkGetDateTime: 0,
    money_fee: 0.000
}

export function orderReducer(state = initState, action) {
    let newState = { ...state };

    if (action.type === GET_DATE_TIME) {
        const { dateTime, checkGetDateTime } = action.payload;

        newState = {
            ...state,
            dateTime: dateTime,
            checkGetDateTime: checkGetDateTime
        }

        return newState;
    } else if (action.type === GET_FEE) {
        const { currentFee, distance } = action.payload;
        newState = {
            ...state,
            currentFee: currentFee,
            distance: distance
        }

        return newState;
    } else if (action.type === GET_MONEY_FEE) {
        const { money_fee } = action.payload;
        newState = {
            ...state,
            money_fee: money_fee
        }

        return newState;
    }
    else {
        return newState;
    }
}