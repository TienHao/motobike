// libary
import AsyncStorage from '@react-native-community/async-storage';
import { applyMiddleware, combineReducers, createStore } from 'redux';
import { persistReducer, persistStore } from 'redux-persist';
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'remote-redux-devtools';
import rootSaga from "../sagas/index";
import { authReducer } from "./auth";
import { slideReducer } from './slide';
import { mapReducer } from './map';
import { orderReducer } from './order';
import { notiReducer } from './noti';

const rootReducer = combineReducers({
  authReducer,
  slideReducer,
  mapReducer,
  orderReducer,
  notiReducer
})

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  whitelist: ["auth"]
}

const persistedReducer = persistReducer(persistConfig, rootReducer)

const composeEnhancers = composeWithDevTools({
  realtime: true,
  name: 'ofo',
  hostname: 'localhost',
  port: 8000,
});

const sagaMiddleware = createSagaMiddleware();
const store = createStore(
  persistedReducer,
  (
    process.env.NODE_ENV === "development"
      ? composeEnhancers(applyMiddleware(sagaMiddleware))
      : applyMiddleware(sagaMiddleware)
  )
)
sagaMiddleware.run(rootSaga);
const persistor = persistStore(store);

export { store, persistor };

