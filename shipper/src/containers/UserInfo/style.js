import { colors } from 'config/colors'
import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.bgBody,
    paddingHorizontal: 16
  },

  btn: {
    backgroundColor: colors.primary.default,
    padding: 16
  }
})
