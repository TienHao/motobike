// libary
import { addUserInfoAction } from 'actions';
import { base, callApi, getApiUpdateUserInfo } from 'api';
import { Toast } from 'components/Toast';
import { MyInputText } from 'controls';
import React, { Component, Fragment } from 'react';
import {
  findNodeHandle,
  Image,
  ScrollView,
  StatusBar,
  Text,
  TouchableOpacity,
  View
} from 'react-native';
import { Icon } from 'react-native-eva-icons';
import ImagePicker from 'react-native-image-picker';
import ImageResizer from 'react-native-image-resizer';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { myRule, permissionType, takePermission } from 'utils';

// styles
import { styles } from './style';

// constantst
import { SHIPPER, CUSTOMMER } from '../../constanst';

const RNFS = require('react-native-fs');

const options = {
  title: 'Chọn hình',
  chooseFromLibraryButtonTitle: "Chọn hình từ thư viện",
  takePhotoButtonTitle: "Chụp hình",
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};


class UserInfoScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Thông tin tài khoản'
    };
  };

  constructor(props) {
    super(props);
    this.state = this._getInitialState();

    this._validForm = [];
    this._positionControls = [];
  }

  _getInitialState = () => {
    const { userInfo } = this.props;

    return {
      userInfo,
      errorMessagePhoto: "",
      isCheckValid: false,
      isUpdateAvatar: false,
      messageToast: "",
      isActiveToast: false,
      toastType: ""
    }
  }

  componentDidMount() {
    this._navListenerFocus = this.props.navigation.addListener('willFocus', () => {
      StatusBar.setTranslucent(true);
      StatusBar.setBarStyle('dark-content');
    });
  }

  componentWillUnmount() {
    this._navListenerFocus.remove();
  }

  _chooseImage = async () => {
    const acceptPhoto = await takePermission(permissionType.photo);
    if (!acceptPhoto) {
      this.setState({ errorMessagePhoto: "Chức năng này cần phải được cho phép truy cập ảnh", isActiveToast: false })
      return
    } else {
      this.setState({ errorMessagePhoto: "", isActiveToast: false });
    }

    ImagePicker.showImagePicker(options, (response) => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        ImageResizer.createResizedImage(response.uri, 400, 400, 'JPEG', 72)
          .then(async (imgResize) => {
            const base64Data = await RNFS.readFile(
              imgResize.path,
              "base64"
            );

            this.setState({
              isUpdateAvatar: true, userInfo: {
                ...this.state.userInfo,
                avatar: `data:image/jpeg;base64,${base64Data}`
              }
            });
          })
          .catch(err => {
            console.log(err);
          });
      }
    })
  }

  _scrollTo = ({ x, y }) => {
    this._scrollView.scrollTo({ x: 0, y, animated: true });
  }

  _calculatePosition = (name) => {
    this._positionControls[name].measureLayout(
      findNodeHandle(this._scrollView),
      (x, y) => {
        this._scrollTo({ x, y });
      }
    )
  }

  _checkValidate = async () => {
    try {
      this._validForm = []
      await this._setStateAsync({ isCheckValid: true });
      this.forceUpdate();

      return {
        valid: this._validForm.every(control => {
          if (!control.valid) {
            this._calculatePosition(control.name)
            return false
          }
          return true
        }),
        data: this.state.userInfo
      }

    } catch (error) {
      return false
    }
  }

  _handleChange = ({ name, value }) => {
    this.setState({
      userInfo: {
        ...this.state.userInfo,
        [name]: value
      },
      isActiveToast: false
    })
  }

  _updateUserInfo = async () => {
    const authorization = this.props.accessToken
    const { valid, data } = await this._checkValidate()

    if (valid) {
      const res = await callApi(getApiUpdateUserInfo({ authorization, data }));

      if (res && res.success) {
        this.props.addUserInfoAction({ userInfo: res.data });
        this.setState({ messageToast: "Cập nhật thành công", isActiveToast: true, toastType: "success" });
      } else {
        this.setState({ messageToast: res.message, isActiveToast: true, toastType: "error" });
      }
    }
  }

  render() {
    const { userInfo, isCheckValid, errorMessagePhoto, isUpdateAvatar } = this.state;
    const type_id = userInfo.type_id;

    return (
      <Fragment>
        <Toast
          type={this.state.toastType}
          active={this.state.isActiveToast}
          message={this.state.messageToast}
        />
        <ScrollView
          style={styles.container}
          ref={el => this._scrollView = el}
        >

          <View style={{ marginTop: 16 }}>
            <View style={{ justifyContent: "center", alignItems: "center" }}>
              <TouchableOpacity
                onPress={() => {
                  let result = userInfo.type_id == CUSTOMMER || userInfo.type_id == undefined ? this._chooseImage() : null;

                  return result;
                }}
              >
                <View
                  style={{ height: 150, width: 150, borderRadius: 75, overflow: "hidden", borderColor: "#A8A8A8", borderStyle: "dashed", borderWidth: 1 }}
                >
                  {
                    userInfo.avatar &&
                    <Image
                      style={{ flex: 1 }}
                      source={{ uri: (isUpdateAvatar && userInfo.avatar || `${base}/${userInfo.avatar}`) }}
                    /> ||
                    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
                      <Icon name={"image-outline"} height={48} width={48} fill={"#A8A8A8"} />
                    </View>
                  }
                </View>
              </TouchableOpacity>
              <View>
                <Text> {errorMessagePhoto}</Text>
              </View>
            </View>
            <View
              ref={el => this._positionControls["fullname"] = el}
            >
              <MyInputText
                name={"fullname"}
                label={"Họ tên"}
                placeholder={"Nhập họ tên"}
                value={userInfo.fullname}
                editable={type_id == SHIPPER ? false : true}
                rules={[myRule.required]}
                onChangeText={this._handleChange}
                checkValidForm={isCheckValid}
                onValidate={(control) => this._validForm.push(control)}
              />
            </View>
            <View
              ref={el => this._positionControls["email"] = el}
            >
              <MyInputText
                name={"email"}
                label={"Email"}
                placeholder={"Nhập email"}
                value={userInfo.email}
                editable={false}
                rules={[myRule.email]}
                checkValidForm={isCheckValid}
                onChangeText={this._handleChange}
                onValidate={(control) => this._validForm.push(control)}
              />
            </View>
            <View
              ref={el => this._positionControls["phone"] = el}
            >
              <MyInputText
                name={"phone"}
                label={"Điện thoại"}
                placeholder={"Nhập số điện thoại"}
                value={userInfo.phone}
                editable={type_id == SHIPPER ? false : true}
                rules={[myRule.required, myRule.phoneNumber]}
                checkValidForm={isCheckValid}
                keyboardType="phone-pad"
                onChangeText={this._handleChange}
                onValidate={(control) => this._validForm.push(control)}
              />
            </View>
            <View>
              <MyInputText
                name={"address"}
                label={"Địa chỉ"}
                editable={type_id == SHIPPER ? false : true}
                placeholder={"Số nhà, tên đường,..."}
                value={userInfo.address}
                onChangeText={this._handleChange}
              />
            </View>
            <View style={{ marginBottom: 20 }}>
              <View>
                <View style={{ flexDirection: 'row' }}>
                  <Text>Điểm tích lũy: </Text>
                  <Text style={{ fontWeight: 'bold', color: 'red', fontSize: 17 }}>{userInfo.PointID === undefined ? 0 : userInfo.PointID}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text>Tiền thưởng: </Text>
                  <Text style={{ fontWeight: 'bold', color: 'red', fontSize: 17 }}>{userInfo.money === undefined ? 0 : userInfo.money}</Text>
                </View>
              </View>
              {userInfo.type_id == SHIPPER ? (
                <View style={{ flexDirection: 'row' }}>
                  <Text>Tiền cọc của shipper: </Text>
                  <Text style={{ fontWeight: 'bold', color: 'red', fontSize: 17 }}>{userInfo.deposit}</Text>
                </View>
              ) : null}
            </View>
          </View>
        </ScrollView>
        {userInfo.type_id == CUSTOMMER || userInfo.type_id == undefined ? (
          <View
            style={styles.btn}
          >
            <TouchableOpacity
              onPress={() => this._updateUserInfo()}
            >
              <Text style={{ textAlign: "center", color: "#fff" }}>Cập nhật</Text>
            </TouchableOpacity>
          </View>
        ) : null}
      </Fragment>
    );
  }
}

function mapStateToProps(state) {
  const { userInfo, accessToken } = state.authReducer
  return { userInfo, accessToken }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ addUserInfoAction }, dispatch)
}

const UserInfoContainer = connect(mapStateToProps, mapDispatchToProps)(UserInfoScreen);
export { UserInfoContainer };

