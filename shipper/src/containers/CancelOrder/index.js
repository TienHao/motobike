// libary
import React, { Component } from 'react';
import { View, Text, TouchableOpacity, FlatList, TouchableWithoutFeedback, ToastAndroid, Image } from 'react-native';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import { connect } from 'react-redux';

// styles
import { styles } from './styles';

// api
import { callApi, getApiHistories } from 'api';

// components
import { Loading } from 'components/Loading';

// utils
import { currencyFormat } from 'utils';

class CancelOrder extends Component {
    constructor(props) {
        super(props);

        this.state = {
            historyCancel: [],
            userInfo: null,
            isLoading: true,
            errorMessage: ''
        }
    }
    componentDidMount() {
        this.getHistoryComplete();
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.userInfo !== this.props.userInfo || prevState.historyCancel.length !== this.state.historyCancel.length) {
            this.getHistoryComplete();
        }
    }

    renderNotOrderComplete = () => {
        return (
            <View style={[styles.container, { margin: 15, alignItems: 'center' }]}>
                <Text style={{ color: 'black', fontWeight: 'bold' }}> Hiện tại chưa có đơn hàng nào đã hủy!</Text>
                <View style={[styles.warpCard]}>
                    <View style={{ justifyContent: 'center', flex: 3, alignItems: 'center' }}>
                        <IconFontAwesome name='cart-plus' style={{ fontSize: 48, color: '#FFA910' }} />
                    </View>
                    <View style={{ flex: 7, justifyContent: 'center', alignItems: 'flex-start' }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate("Order")}>
                            <Text style={{ fontSize: 20, fontWeight: 'bold' }}>Tạo đơn hàng mới</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }

    getHistoryComplete = async () => {
        const { userInfo } = this.props;

        if (userInfo) {
            const authorization = this.props.accessToken;
            const res = await callApi(getApiHistories({ authorization, userId: userInfo.id, typeId: 0 }));


            if (res.success) {
                const histories = res.data;

                let historyCancel = [];

                histories.forEach(function (history) {
                    if (history.status_id === '4') {
                        historyCancel.push(history);
                    }
                });

                this.setState({ userInfo, historyCancel: historyCancel, isLoading: false });
            } else {
                this.setState({ errorMessage: "Xảy ra lỗi", isLoading: true });
            }

        } else {
            this.setState({ errorMessage: "Bạn chưa đăng nhập", isLoading: true });
        }
    }

    renderHistoryItem = ({ item }) => {
        return (
            <View style={{ backgroundColor: 'white', marginBottom: 15 }}>
                <TouchableWithoutFeedback onPress={() => this.onPressItem(item)}>
                    <View style={{ padding: 15 }}>
                        <View style={{ flexDirection: 'row' }}>
                            <View>
                                <Text>{item.code}</Text>
                            </View>
                            <View style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'flex-end' }}>
                                <Text style={{ fontWeight: 'bold', fontSize: 18 }}>{currencyFormat(item.fee)} VNĐ</Text>
                            </View>
                        </View>

                        <View style={{ marginTop: 10, marginBottom: 10 }}>
                            <View>
                                <Text>{item.address_from}</Text>
                            </View>
                            <View>
                                <Text>{item.address_to}</Text>
                            </View>
                        </View>
                        <View style={{ marginBottom: 5, flexDirection: "row" }}>
                            <View>
                                <Image
                                    resizeMode='stretch'
                                    source={{ uri: item.image }}
                                    style={{ height: 100, width: 120 }}
                                />
                            </View>
                            <View style={{ flex: 1, marginTop: 10, justifyContent: 'flex-end', alignItems: 'flex-end' }}>
                                <Text style={{ fontWeight: 'bold', color: 'grey' }}>{item.status_name}</Text>
                            </View>
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </View>
        )
    }

    onPressItem = (item) => {
        this.props.navigation.navigate('HistoryDetail', { history: { ...item } });
    }

    cancelOrder = (item) => {
        this.props.navigation.navigate("CancelOrder", { history: item });
    }

    render() {
        const { isLoading, errorMessage } = this.state;
        console.log('userInfo', this.props.userInfo);
        return (
            <React.Fragment>
                {isLoading &&
                    <View style={{ flex: 1, justifyContent: "center" }}>
                        {!errorMessage
                            && <Loading />
                            || <Text style={{ textAlign: "center" }}>{errorMessage}</Text>
                        }
                    </View> || (
                        <View style={styles.container}>
                            {
                                this.state.historyCancel.length ?
                                    <FlatList
                                        initialNumToRender={10}
                                        maxToRenderPerBatch={4}
                                        updateCellsBatchingPeriod={5000}
                                        data={this.state.historyCancel}
                                        extraData={this.state}
                                        numColumns={1}
                                        keyExtractor={(item, index) => `${item.id}-${index}`}
                                        renderItem={this.renderHistoryItem}
                                    />
                                    :
                                    this.renderNotOrderComplete()
                            }

                        </View>
                    )}
            </React.Fragment>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        accessToken: state.authReducer.accessToken,
        userInfo: state.authReducer.userInfo
    }
}

const mapDispatchToProps = {

}

export default connect(mapStateToProps, mapDispatchToProps)(CancelOrder);