// libary
import { GoogleSignin } from '@react-native-community/google-signin';
import { getUserInfoAction, removeUserInfoAction } from 'actions';
import React, { PureComponent } from 'react';
import { StatusBar, Text, TouchableWithoutFeedback, View } from 'react-native';
import { Icon } from 'react-native-eva-icons';
import { AccessToken, LoginManager } from 'react-native-fbsdk';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import IconMaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

// styles
import { styles } from './style';

class AccountScreen extends PureComponent {
  constructor(props) {
    super(props)

    this.state = this._getInitialState();
    this._initialize();
  }

  _getInitialState = () => {
    return {
      isLoginFacebook: false,
      isLoginGoogle: false,
      isLoginLocal: false
    }
  }

  _initialize = () => {
    StatusBar.setBarStyle('dark-content');
    StatusBar.setTranslucent(true);

    this._registerGoogleConfig();
  }

  componentDidMount() {
    this._navListenerFocus = this.props.navigation.addListener('willFocus', () => {
      StatusBar.setTranslucent(true);
      StatusBar.setBarStyle('dark-content');
    });
  }

  componentWillUnmount() {
    this._navListenerFocus.remove();
  }

  _registerGoogleConfig = () => {
    GoogleSignin.configure({
      iosClientId: "373203814436-ko224h75pij5l63nto49vdvne6vssokm.apps.googleusercontent.com",
      webClientId: "373203814436-ko224h75pij5l63nto49vdvne6vssokm.apps.googleusercontent.com",
      offlineAccess: true,
      forceConsentPrompt: true
    })
  }

  _getCurrentUserGoogle = async () => {
    const token = await GoogleSignin.getCurrentUser();
    if (token) this.setState({ isLoginGoogle: true });
  };

  _getCurrentUserFacebook = async () => {
    const token = await AccessToken.getCurrentAccessToken();
    if (token) this.setState({ isLoginFacebook: true });
  };

  _signOut = async () => {
    this.props.removeUserInfoAction();
    await GoogleSignin.revokeAccess();
    await GoogleSignin.signOut();
    await LoginManager.logOut();
  }

  _categoryItem = ({ item, index }) => {
    return (
      item.active(this.props.userInfo) &&
      <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate(`${item.navigation}`)}>
        <View style={{ flex: 1, padding: 8, marginBottom: 4 }}>
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <View style={{ padding: 5, backgroundColor: `${item.bgIconColor}`, borderRadius: 44 / 2 }}>
              <Icon name={item.icon} width={12} height={12} fill={'#fff'}></Icon>
            </View>
            <View style={{ marginLeft: 8 }}>
              <Text style={{ fontSize: 18 }}>{item.text}</Text>
            </View>
          </View>
        </View>
      </TouchableWithoutFeedback> || null
    )
  }

  _renderButton = () => {
    const { userInfo } = this.props;

    if (!userInfo) {
      return (
        <View>
          <View style={{ marginBottom: 16, borderBottomWidth: 1, borderColor: 'grey' }}>
            <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('Register')}>
              <View style={{ flexDirection: "row", alignItems: "center", paddingBottom: 10 }}>
                <IconMaterialCommunityIcons name="account-key" style={{ fontSize: 24, color: 'grey' }} />
                <View style={{ marginLeft: 8 }}>
                  <Text style={{ fontSize: 18 }}>Đăng ký</Text>
                </View>
              </View>
            </TouchableWithoutFeedback>
          </View>
          <View style={{ marginBottom: 16, borderBottomWidth: 1, borderColor: 'grey' }}>
            <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('Auth')}>
              <View style={{ flexDirection: "row", alignItems: "center", paddingBottom: 10 }}>
                <IconMaterialCommunityIcons name="key-plus" style={{ fontSize: 24, color: 'grey' }} />
                <View style={{ marginLeft: 8 }}>
                  <Text style={{ fontSize: 18 }}>Đăng nhập</Text>
                </View>
              </View>
            </TouchableWithoutFeedback>
          </View>
        </View>
      )
    }

    return (
      <TouchableWithoutFeedback onPress={() => this._signOut()}>
        <View style={{ flexDirection: "row", alignItems: "center", borderBottomWidth: 1, borderColor: 'grey' }}>
          <IconFontAwesome name="sign-out" style={{ fontSize: 24, color: 'grey', paddingBottom: 10 }} />
          <View style={{ marginLeft: 8 }}>
            <Text style={{ fontSize: 18, paddingBottom: 10, marginLeft: 5 }}>Đăng xuất</Text>
          </View>
        </View>
      </TouchableWithoutFeedback>
    )
  }

  render() {
    const { userInfo } = this.props;

    return (
      <React.Fragment>
        <View style={[{ flexDirection: 'row', marginTop: getStatusBarHeight(), height: 45, borderWidth: 0, elevation: 5, backgroundColor: 'white', shadowOpacity: 5, shadowColor: 'grey', marginBottom: 16 }]}>

          <View style={{ justifyContent: 'center', flex: 1, alignItems: 'center' }}>
            <Text style={{ fontSize: 20 }}>Tài khoản</Text>
          </View>
        </View>

        <View style={styles.container}>
          <View>
            {
              userInfo &&
              <>
                <View style={{ marginBottom: 16, borderBottomWidth: 1, borderColor: 'grey' }}>
                  <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('UserInfo')}>
                    <View style={{ flexDirection: 'row' }}>
                      <IconMaterialCommunityIcons style={{ fontSize: 24, color: 'grey' }} name="account-edit" />
                      <View style={{ flexDirection: "row", alignItems: "center" }}>
                        <View style={{ marginLeft: 8 }}>
                          <Text style={{ fontSize: 18, paddingBottom: 10, marginLeft: 5 }}>Thông tin tài khoản</Text>
                        </View>
                      </View>
                    </View>
                  </TouchableWithoutFeedback>
                </View>
                <View style={{ marginBottom: 16, borderBottomWidth: 1, borderColor: 'grey' }}>
                  <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('ChangePass')}>
                    <View style={{ flexDirection: 'row' }}>
                      <IconMaterialCommunityIcons name="account-key" style={{ fontSize: 24, color: 'grey' }} />
                      <View style={{ flexDirection: "row", alignItems: "center", marginLeft: 10 }}>
                        <View style={{ marginLeft: 8 }}>
                          <Text style={{ fontSize: 18, paddingBottom: 10 }}>Đổi mật khẩu</Text>
                        </View>
                      </View>
                    </View>
                  </TouchableWithoutFeedback>
                </View>
              </>
              || null
            }
            <View style={{ marginBottom: 16, borderBottomWidth: 1, borderColor: 'grey' }}>
              <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('Contact')}>
                <View style={{ flexDirection: 'row' }}>
                  <IconAntDesign name="contacts" style={{ fontSize: 24, color: 'grey' }} />
                  <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <View style={{ marginLeft: 8 }}>
                      <Text style={{ fontSize: 18, paddingBottom: 10, marginLeft: 5 }}>Liên hệ</Text>
                    </View>
                  </View>
                </View>
              </TouchableWithoutFeedback>
            </View>
          </View>
          <View style={{ borderRadius: 16 }}>
            {this._renderButton()}
          </View>
        </View>
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  const { userInfo } = state.authReducer;
  return { userInfo }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ getUserInfoAction, removeUserInfoAction }, dispatch)
}

const AccountContainer = connect(mapStateToProps, mapDispatchToProps)(AccountScreen);
export { AccountContainer };

