import { colors } from 'config/colors'
import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    marginLeft: 16,
  },

  btnLogin: {
    justifyContent: "center",
    marginVertical: 16,
    backgroundColor: colors.primary.default,
    padding: 16,
    borderRadius: 8,
    textAlign: "center",
    color: "#fff"
  },

  btnLogout: {
    justifyContent: "center",
    marginVertical: 16,
    borderColor: colors.primary.default,
    borderWidth: 1,
    padding: 16,
    borderRadius: 8,
    textAlign: "center",
    color: colors.primary.default
  }
})
