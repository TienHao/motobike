import { callApi, getApiResetPass } from 'api';
import { Toast } from 'components/Toast';
import { MyInputText } from 'controls';
import OfoTextInput from '../../components/OfoTextInput';
import React, { PureComponent } from 'react';
import { findNodeHandle, ScrollView, Text, TouchableOpacity, View } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { myRule } from 'utils';
import { styles } from './style';

class ResetPassScreen extends PureComponent {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Đổi mật khẩu'
    };
  };

  constructor(props) {
    super(props)

    this._validForm = []
    this._positionControls = []

    this.state = this._getInitialState()
  }

  _getInitialState = () => {
    return {
      isCheckValid: false,
      email: "",
      toastType: "",
      isActiveToast: false,
      messageToast: ""
    }
  }

  _handleChange = ({ name, value }) => {
    this.setState({ [name]: value, isActiveToast: false })
  }

  _scrollTo = ({ x, y }) => {
    this._scrollView.scrollTo({ x: 0, y, animated: true })
  }

  _calculatePosition = (name) => {
    this._positionControls[name].measureLayout(
      findNodeHandle(this._scrollView),
      (x, y) => {
        this._scrollTo({ x, y })
      }
    )
  }

  _checkValidate = async () => {
    try {
      this._validForm = []
      await this._setStateAsync({ isCheckValid: true })
      this.forceUpdate()

      return {
        valid: this._validForm.every(control => {
          if (!control.valid) {
            this._calculatePosition(control.name)
            return false
          }
          return true
        }),
        data: {
          email: this.state.email
        }
      }

    } catch (error) {
      console.log('ResetPass', { error })
      return false
    }
  }

  _resetPass = async () => {
    const authorization = this.props.accessToken
    const { valid, data } = await this._checkValidate()

    if (valid) {
      const res = await callApi(getApiResetPass({ authorization, data }))

      if (res && res.success) {
        this.setState({ messageToast: "Cập nhật thành công", isActiveToast: true, toastType: "success" }, () =>
          setTimeout(() => {
            this.props.navigation.pop()
          }, 2000)
        )
      } else {
        this.setState({ messageToast: res.message.error_message, isActiveToast: true, toastType: "error" })
      }
    }
  }

  render() {
    const { isCheckValid } = this.state
    return (
      <>
        <Toast
          type={this.state.toastType}
          active={this.state.isActiveToast}
          message={this.state.messageToast}
        />
        <ScrollView
          style={styles.container}
          ref={el => this._scrollView = el}
        >
          <View
            ref={el => this._positionControls["email"] = el}
          >
            <MyInputText
              name={"email"}
              label={"Email"}
              placeholder={"Nhập email"}
              value={this.state.email}
              rules={[myRule.required, myRule.email]}
              checkValidForm={isCheckValid}
              onChangeText={this._handleChange}
              onValidate={(control) => this._validForm.push(control)}
            />
          </View>

          <View
            style={styles.btn}
          >
            <TouchableOpacity
              onPress={() => this._resetPass()}
            >
              <Text style={{ textAlign: "center", color: "#fff" }}>Reset mật khẩu</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </>
    )
  }
}


function mapStateToProps(state) {
  const { accessToken } = state.authReducer
  return { accessToken }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch)
}

const ResetPassContainer = connect(mapStateToProps, mapDispatchToProps)(ResetPassScreen);
export { ResetPassContainer };

