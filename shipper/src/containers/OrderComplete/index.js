// libary
import React, { Component } from 'react';
import { View, Text, TouchableOpacity, FlatList, TouchableWithoutFeedback, Image } from 'react-native';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import { connect } from 'react-redux';

// styles
import { styles } from './styles';

// api
import { callApi, getApiHistories } from 'api';

// components
import { Loading } from 'components/Loading';

// constanst
import { SHIPPER, CUSTOMMER } from '../../constanst';

// utils
import { currencyFormat } from 'utils';

class OrderComplete extends Component {
    constructor(props) {
        super(props);

        this.state = {
            historyComplete: [],
            userInfo: null,
            isLoading: true,
            errorMessage: ''
        }
    }
    componentDidMount() {
        this.getHistoryComplete();

        this.focusListener = this.props.navigation.addListener('willFocus', () => {
            this.getHistoryComplete();
        });
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.userInfo !== this.props.userInfo || prevState.historyComplete.length !== this.state.historyComplete.length) {
            this.getHistoryComplete();
        }
    }

    renderNotOrderComplete = () => {
        return (
            <View style={[styles.container, { margin: 15 }]}>
                <Text style={{ color: 'black', fontWeight: 'bold' }}> Hiện tại chưa có đơn hàng nào đã hoàn thành!</Text>
                <View style={[styles.warpCard]}>
                    <View style={{ justifyContent: 'center', flex: 3, alignItems: 'center' }}>
                        <IconFontAwesome name='cart-plus' style={{ fontSize: 48, color: '#FFA910' }} />
                    </View>
                    <View style={{ flex: 7, justifyContent: 'center', alignItems: 'flex-start' }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate("Order")}>
                            <Text style={{ fontSize: 20, fontWeight: 'bold' }}>Tạo đơn hàng mới</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }

    getHistoryComplete = async () => {
        const { userInfo } = this.props;

        if (userInfo) {
            const authorization = this.props.accessToken;

            let res = userInfo.type_id == SHIPPER ? await callApi(getApiHistories({ authorization, userId: userInfo.id, typeId: 1 })) : await callApi(getApiHistories({ authorization, userId: userInfo.id, typeId: 0 }));

            if (res.success) {
                const histories = res.data;

                let historyComplete = [];

                histories.forEach(function (history) {
                    if (history.status_id === '3' && userInfo.type_id == CUSTOMMER || userInfo.type_id === undefined) {
                        historyComplete.push(history);
                    } else if (userInfo.type_id == SHIPPER && history.status_id === '2') {
                        historyComplete.push(history);
                    }
                });

                this.setState({ userInfo, historyComplete: historyComplete, isLoading: false });
            } else {
                this.setState({ errorMessage: "Xảy ra lỗi", isLoading: true });
            }

        } else {
            this.setState({ errorMessage: "Bạn chưa đăng nhập", isLoading: true });
        }
    }

    renderHistoryItem = ({ item, index }) => {
        return (
            <View style={{ backgroundColor: 'white', marginBottom: 15 }}>
                <TouchableWithoutFeedback onPress={() => this.onPressItem(item)}>
                    <View style={{ padding: 15 }}>
                        <View style={{ flexDirection: 'row' }}>
                            <View>
                                <Text>{item.code}</Text>
                            </View>
                            <View style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'flex-end' }}>
                                <Text style={{ fontWeight: 'bold', fontSize: 18 }}>{currencyFormat(item.fee)} VNĐ</Text>
                            </View>
                        </View>
                        <View style={{ marginTop: 10, marginBottom: 10 }}>
                            <View>
                                <Text>{item.address_from}</Text>
                            </View>
                            <View>
                                <Text>{item.address_to}</Text>
                            </View>
                        </View>

                        <View style={{ marginBottom: 5, flexDirection: "row" }}>
                            <View>
                                <Image
                                    resizeMode='stretch'
                                    source={{ uri: item.image }}
                                    style={{ height: 100, width: 120 }}
                                />
                            </View>
                            <View style={{ flex: 1, marginTop: 10, justifyContent: 'flex-end', alignItems: 'flex-end' }}>
                                <Text style={{ fontWeight: 'bold', color: 'grey' }}>{item.status_name}</Text>
                            </View>
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </View>
        )
    }

    onPressItem = (item) => {
        this.props.navigation.navigate('HistoryDetail', { history: { ...item } });
    }

    cancelOrder = (item) => {
        this.props.navigation.navigate("CancelOrder", { history: item });
    }

    render() {
        const { isLoading, errorMessage } = this.state;
        const { userInfo } = this.props;

        return (
            <React.Fragment>
                {isLoading &&
                    <View style={{ flex: 1, justifyContent: "center" }}>
                        {!errorMessage
                            && <Loading />
                            || <Text style={{ textAlign: "center" }}>{errorMessage}</Text>
                        }
                    </View> || (
                        <View style={styles.container}>
                            {
                                this.state.historyComplete.length ?
                                    <FlatList
                                        initialNumToRender={10}
                                        maxToRenderPerBatch={4}
                                        updateCellsBatchingPeriod={5000}
                                        data={this.state.historyComplete}
                                        extraData={this.state}
                                        numColumns={1}
                                        keyExtractor={(item, index) => `${item.id}-${index}`}
                                        renderItem={this.renderHistoryItem}
                                    />
                                    :
                                    userInfo.type_id === SHIPPER ? (
                                        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                                            <Text style={{ color: 'black', fontWeight: 'bold' }}>Hiện tại bạn chưa có đơn hàng đã chọn!</Text>
                                        </View>
                                    ) : this.renderNotOrderComplete()
                            }

                        </View>
                    )}
            </React.Fragment>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        accessToken: state.authReducer.accessToken,
        userInfo: state.authReducer.userInfo
    }
}

const mapDispatchToProps = {

}

export default connect(mapStateToProps, mapDispatchToProps)(OrderComplete);