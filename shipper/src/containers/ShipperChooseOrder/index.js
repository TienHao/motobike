// libary
import { callApi, getApiChooseOrder, updateStatusDelivery } from 'api';
import React, { PureComponent } from 'react';
import {
    findNodeHandle,
    ScrollView,
    Text,
    TouchableOpacity,
    View,
    ToastAndroid
} from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import AsyncStorage from '@react-native-community/async-storage';

// components
import { Toast } from 'components/Toast';

// colors
import { colors } from 'config/colors';

// controls
import { MyInputText } from 'controls';

// actions
import { changeStatusPushNotification } from '../../actions';

// constanst
import { CHANGE_STATUS_CHOOSE_ORDER_PUSH_NOTI } from '../../constanst';


class ShipperChooseOrderScreen extends PureComponent {
    static navigationOptions = ({ navigation }) => {
        return {
            title: 'Shipper Chọn đơn hàng'
        };
    };

    constructor(props) {
        super(props);

        this._positionControls = [];

        this._history = this.props.navigation.getParam('history', 'some default value');
        this.state = this._getInitialState();
    }

    _getInitialState = () => ({
        ...this._history,
    })

    _handleChange = ({ name, value }) => {
        this.setState({ [name]: value })
    }

    _scrollTo = ({ x, y }) => {
        this._scrollView.scrollTo({ x: 0, y, animated: true })
    }

    _calculatePosition = (name) => {
        this._positionControls[name].measureLayout(
            findNodeHandle(this._scrollView),
            (x, y) => {
                this._scrollTo({ x, y })
            }
        )
    }

    updateStatusOrder = async (code) => {
        const authorization = this.props.accessToken;

        await callApi(updateStatusDelivery({ authorization, data: { code: code, status_id: 2 } }));
    }

    chooseOrder = async () => {
        const authorization = this.props.accessToken;
        const { userInfo } = this.props;

        const { code } = this.state;

        const data = {
            code: code,
            member_id: userInfo.id
        };

        const res = await callApi(getApiChooseOrder({ authorization, data }));

        if (res && res.success) {
            try {
                await this.updateStatusOrder(code);
                this.props.changeStatusPushNotification({ valueCheckPushNoti: 1 }, CHANGE_STATUS_CHOOSE_ORDER_PUSH_NOTI);

                await AsyncStorage.setItem(code, JSON.stringify(data));
                await this.props.navigation.navigate("Home", { cancelOrder: true });
            } catch (error) {
                console.log('error', error);
            }


        } else {
            this.setState({ messageToast: "Có lỗi xảy ra", isActiveToast: true, toastType: "error" });
        }
    }

    _timeEnd = () => {
        this.setState({ isActiveToast: false, messageToast: "" });
    }

    render() {

        return (
            <>
                <ScrollView
                    ref={el => this._scrollView = el}
                    style={{ flex: 1, paddingHorizontal: 16, backgroundColor: colors.bgBody, paddingTop: 16 }}
                >

                    <View>
                        <MyInputText
                            name={"code"}
                            label={"Mã vận đơn"}
                            value={this.state.code}
                            editable={false}
                        />
                    </View>
                    <View>
                        <TouchableOpacity
                            onPress={() => this.chooseOrder()}
                        >
                            <View
                                style={{
                                    backgroundColor: colors.primary.default,
                                    padding: 16
                                }}
                            >
                                <Text style={{ textAlign: "center", color: "#fff" }}>Chọn đơn hàng</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </>
        )
    }
}

function mapStateToProps(state) {
    const { accessToken, userInfo } = state.authReducer;
    return { accessToken, userInfo };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ changeStatusPushNotification }, dispatch);
}

const ShipperChooseOrderContainer = connect(mapStateToProps, mapDispatchToProps)(ShipperChooseOrderScreen);
export { ShipperChooseOrderContainer };

