// libary
import { colors } from 'config/colors';
import React, { Fragment, PureComponent } from 'react';
import {
    ActivityIndicator,
    Text,
    View,
    TouchableNativeFeedback,
    Image,
    BackHandler
} from 'react-native';
import Geolocation from 'react-native-geolocation-service';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { permissionType, takePermission } from 'utils';
import Geocoder from 'react-native-geocoder';
import LocationServicesDialogBox from "react-native-android-location-services-dialog-box";

// styles
import styles from './styles';

// actions
import { getAddressDelivery } from '../../actions';

Geocoder.fallbackToGoogle("AIzaSyCx3c8GVP57CDXvA8l1iCNRlMFrXdj7G_0");

class MapLocation extends PureComponent {
    static navigationOptions = ({ navigation }) => {
        return {
            title: "Chọn địa chỉ"
        }
    }

    constructor(props) {
        super(props);

        this.state = {
            permissionMessage: "",
            permissionLocation: false,
            isGetCurrentLocation: false,
            isInitialRegion: true,
            currentRegion: null,
            currentLocation: {
                latitude: 0,
                longitude: 0,
                latitudeDelta: 0.02,
                longitudeDelta: 0.02,
            },
            address: '',
        }
    }

    componentDidMount() {
        const accept = takePermission(permissionType.location);

        LocationServicesDialogBox.checkLocationServicesIsEnabled({
            message: "<h5>Bật gps để sử dụng định vị location ?<br/>",
            ok: "YES",
            cancel: "NO",
            enableHighAccuracy: true,
            showDialog: true,
            openLocationServices: true,
            preventOutSideTouch: false,
            preventBackClick: false,
            providerListener: false
        }).then(function (success) {
            console.log(success);
        }).catch((error) => {
            console.log(error.message);
        });

        BackHandler.addEventListener('hardwareBackPress', () => {
            LocationServicesDialogBox.forceCloseDialog();
        });

        if (accept) {
            this.setState({ permissionLocation: true });
            this.getCurrentLocation();
        } else {

            this.setState({ permissionMessage: "Bạn cần phải cho phép truy cập vị trí để sử dụng chức năng này" })
        }
    }

    componentWillUnmount() {

    }

    getCurrentLocation = () => {
        Geolocation.getCurrentPosition(
            (position) => {
                const { coords } = position;

                Geocoder.geocodePosition({ lat: coords.latitude, lng: coords.longitude })
                    .then(res => {
                        this.setState({
                            address: res[0].formattedAddress
                        })
                    })
                    .catch(err => console.log(err));

                this.setState({
                    currentLocation: {
                        ...this.state.currentLocation,
                        latitude: coords.latitude,
                        longitude: coords.longitude
                    },
                    isGetCurrentLocation: true
                })
            },
            (error) => {
                this.setState({ permissionLocation: false });
                this.setState({ permissionMessage: "Request time out!" });
            },
            { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
        );
    }

    onRegionChange = async (region) => {
        await this.setState({
            currentRegion: {
                longitude: region.longitude,
                latitude: region.latitude,
                latitudeDelta: region.latitudeDelta,
                longitudeDelta: region.longitudeDelta
            }
        });
    }

    onDragEndGetLocation = (location) => {
        Geocoder.geocodePosition({ lat: location.latitude, lng: location.longitude })
            .then(res => {
                this.setState({
                    address: res[0].formattedAddress
                })
            })
            .catch(err => console.log(err));
    }

    postAddress = () => {
        if (this.props.valueCheckPeople === 1) {
            this.props.getAddressDelivery({ addressDeliver: this.state.address });
            this.props.navigation.navigate("Order");
        } else if (this.props.valueCheckPeople === 2) {
            this.props.getAddressDelivery({ addressReceiver: this.state.address });
            this.props.navigation.navigate("Order");
        }
    }

    renderMarker = (currentLocation, address) => {
        return (
            <MapView.Marker
                draggable

                showsUserLocation={true}
                coordinate={{
                    latitude: currentLocation.latitude,
                    longitude: currentLocation.longitude
                }}
                onDragEnd={(e) => this.onDragEndGetLocation(e.nativeEvent.coordinate)}
                title={"Vị trí hiện tại"}
                description={address !== '' ? address : "mô tả"}
            >
                <Image
                    resizeMode="stretch"
                    style={{ width: 40, height: 50 }}
                    source={require('../../images/icon-marker.png')}
                />
            </MapView.Marker>
        );
    }

    render() {
        let {
            isGetCurrentLocation,
            currentLocation,
            isInitialRegion,
            permissionMessage,
            address
        } = this.state;

        return (
            <View style={styles.containerMap}>
                {
                    isGetCurrentLocation
                    &&
                    <Fragment>
                        <MapView
                            rotateEnabled={false}
                            ref={ref => this.mapRef = ref}
                            provider={PROVIDER_GOOGLE}
                            style={styles.map}
                            showsUserLocation={true}
                            showsMyLocationButton={true}
                            followsUserLocation={true}
                            initialRegion={isInitialRegion ? currentLocation : undefined}
                            onRegionChange={this.onRegionChange}
                            cacheEnabled
                        >
                            {this.renderMarker(currentLocation, address)}
                        </MapView>
                        <View style={{ backgroundColor: 'white', elevation: 2, borderWidth: 0, padding: 15, marginTop: 10, marginLeft: 10, marginRight: 10 }}>
                            <Text style={{ fontWeight: 'bold', fontSize: 16, textAlign: 'center' }}>{this.state.address}</Text>
                        </View>
                    </Fragment>
                    ||
                    <View
                        style={styles.container}
                    >
                        {
                            !permissionMessage
                            && <ActivityIndicator size="large" color={colors.primary.default} />
                            ||
                            <View style={{ justifyContent: "center", alignItems: "center", paddingHorizontal: 16 }}>
                                <Text style={{ textAlign: "center" }}>{permissionMessage}</Text>
                            </View>
                        }

                    </View>
                }
                <View style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'flex-end', marginBottom: 10 }}>
                    <TouchableNativeFeedback
                        onPress={() => this.postAddress()}
                    >
                        <Text style={{
                            padding: 12,
                            color: "#fff",
                            backgroundColor: "#FCB300",
                            textAlign: "center",
                            width: 200
                        }}>
                            {"Xác Nhận"}
                        </Text>
                    </TouchableNativeFeedback>
                </View>
            </View>
        );
    }
}

function mapStateToProps(state) {
    return {
        valueCheckPeople: state.mapReducer.valueCheckPeople
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ getAddressDelivery }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(MapLocation);