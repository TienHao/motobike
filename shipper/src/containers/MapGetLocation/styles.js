import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    containerMap: {
        ...StyleSheet.absoluteFillObject,
        alignItems: 'center',
    },
    map: {
        ...StyleSheet.absoluteFillObject,
        flex: 1
    },
    container: {
        justifyContent: "center",
        alignItems: "center",
        flex: 1
    },

    containerControl: {
        position: 'absolute',
        bottom: 0,
        alignSelf: 'center',
        flex: 1
    },

    containerBack: {
        position: 'absolute',
        top: 50,
        left: 20,
        backgroundColor: "rgba(255, 255, 255, 0.5)",
        padding: 16,
        borderRadius: 16
    },

    btn: {
        padding: 16,
        borderRadius: 8,
        backgroundColor: "#fff",
        textAlign: "center",
    },
})

export default styles;