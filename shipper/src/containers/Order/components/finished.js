import React, { PureComponent } from 'react';
import { Text, TouchableOpacity, View } from 'react-native';

class FinishStep extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      ...this.getInitialState(),
      ...this.props.getDataByStep(5),
      ...this.props.getDataByStep(4)
    };
  }

  getInitialState = () => {
    return {};
  }

  checkValidate = async () => {
    try {
      return {
        valid: true,
        data: {}
      };
    } catch (error) {
      return false;
    }
  }

  setCurrentStep = () => {
    this.props.callbackSetCurrentStep();
    this.props.getAddressDelivery({ addressDeliver: '' });

    this.props.getAddressDelivery({ addressReceiver: '' });
    
    this.props.getFee({
      currentFee: undefined,
      distance: undefined
    });

    this.props.navigation.goBack();
  }

  goHome = () => {
    this.props.getAddressDelivery({ addressDeliver: '' });
    this.props.getAddressDelivery({ addressReceiver: '' });
    this.props.getFee({
      currentFee: undefined,
      distance: undefined
    });
    this.props.navigation.navigate("Home");
  }

  render() {
    const { responseData } = this.state;

    return (
      <View style={{ flex: 1, justifyContent: "flex-start", alignItems: "center" }}>
        <Text style={{ fontSize: 16, marginBottom: 8 }}>Đơn hàng của bạn đã tạo thành công! </Text>
        <Text style={{ marginBottom: 8 }}>Xin chân thành cám ơn đã tin tưởng Ofo!</Text>
        <Text style={{ fontSize: 20, textAlign: "center", marginVertical: 32 }}>Mã đơn hàng{"\n"}{responseData}</Text>
        <Text style={{ fontSize: 12 }}>Vui lòng lưu trữ lại mã này để tra cứu trạng thái đơn hàng.</Text>
        <View
          style={{ backgroundColor: "#FCB300", padding: 10, marginTop: 40, width: 180 }}
        >
          <TouchableOpacity
            style={{ justifyContent: 'center', alignItems: 'center' }}
            onPress={() => this.setCurrentStep()}
          >
            <Text style={{ color: "#fff" }}>
              Tiếp tục tạo đơn hàng
            </Text>
          </TouchableOpacity>
        </View>
        <View
          style={{ backgroundColor: "#FCB300", padding: 10, marginTop: 20, width: 180 }}
        >
          <TouchableOpacity
            style={{ justifyContent: 'center', alignItems: 'center' }}
            onPress={() => this.goHome()}
          >
            <Text style={{ color: "#fff" }}>
              Quay về trang chủ
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

export { FinishStep };

