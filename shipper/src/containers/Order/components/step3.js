// libary
import { callApi, getDeliveryFee } from 'api';
import { Loading } from 'components/Loading';
import React, { PureComponent } from 'react';
import {
  ScrollView,
  Text,
  View,
  TextInput,
  findNodeHandle
} from 'react-native';
import { styles } from "../style";
import { GoogleMap } from './GoogleMap';

class OrderStep3 extends PureComponent {
  constructor(props) {
    super(props);

    this.positionControls = [];

    this.state = {
      ...this.getInitialState(),
      step2: { ...this.props.getDataByStep(2) },
      isErrorPrice: false
    }
  }

  componentDidMount() {
    this.initializeAsync();
  }

  async initializeAsync() {
    const authorization = this.props.accessToken;

    const step2 = this.props.getDataByStep(2);

    const api = getDeliveryFee({
      step1: this.props.getDataByStep(1),
      step2: step2,
      authorization
    });

    const data = {
      authorization: api.authorization,
      body: [
        {
          "address_from": api.body[0].address_from,
          "address_to": api.body[0].address_to
        },
        {
          "mass": api.body[1].mass,
          "money_fee": 0.000
        }
      ],
      method: "POST",
      url: "http://motobike.vn/api/delivery/fee"
    }

    const res = await callApi(data);

    await this.props.getFee({ currentFee: res.data.fee, distance: res.data.distance });

    if (res.success) {
      await this._setStateAsync({ result: { ...res.data, fee: res.data.fee }, isLoading: false });
    }
  }

  getInitialState = () => {
    return {
      isLoading: true,
      result: {},
      current_fee: 0.000
    }
  }

  checkValidate = async () => {

    try {
      return {
        valid: !this.state.isLoading,
        data: {
          step1: { ...this.props.getDataByStep(1) },
          step2: { ...this.props.getDataByStep(2) },
          ...this.state
        }
      }
    } catch (error) {
      return false;
    }
  }

  scrollTo = ({ x, y }) => {
    this._scrollView.scrollTo({ x: 0, y, animated: true });
  }

  calculatePosition = (name) => {
    this.positionControls[name].measureLayout(
      findNodeHandle(this._scrollView),
      (x, y) => {
        this.scrollTo({ x, y });
      }
    )
  }

  onChangeTextMoneyFee = (price) => {
    const { userInfo } = this.props;
    const { fee } = this.state.result;

    if ((userInfo.money <= price && price > 0) || (price <= fee && price > 0)) {
      this.setState({
        isErrorPrice: true,
        result: {
          ...this.state.result,
          money_fee: 0
        }
      })
    } else {
      this.setState({
        isErrorPrice: false,
        result: {
          ...this.state.result,
          money_fee: price
        }
      })
    }
  }

  componentWillUnmount() {
    const { result } = this.state;

    this.props.getMoneyFee({ money_fee: result.money_fee });
  }

  render() {
    const { step2, isLoading, money_fee } = this.state;

    const { fee } = this.state.result;

    const { userInfo } = this.props;

    if (isLoading) {
      return (
        <View style={{ flex: 1, justifyContent: "center" }}>
          <Loading />
        </View>
      )
    }

    return (
      <ScrollView
        style={{ marginBottom: 100 }}
        ref={el => this._scrollView = el}
      >
        <View style={{ height: 200 }}>
          <GoogleMap
            address={step2.responseData.places}
            origin={step2.responseData.location.origin}
            destination={step2.responseData.location.destination}
          />
        </View>
        <View style={styles.containerForm}>
          <View>
            <Text style={styles.titleBlock}>
              Tiền phải trả
            </Text>
            <View style={[styles.row, { paddingBottom: 8, marginBottom: 8, borderColor: "#f0f0f0", borderBottomWidth: 1 }]}>
              <View style={styles.col}>
                <Text>Cước phí hiện tại</Text>
              </View>
              <View style={styles.col}>
                <Text style={{ color: 'red', fontWeight: 'bold', fontSize: 16 }}>{fee || 0} VNĐ</Text>
              </View>
            </View>
            <View style={[{ flex: 1, flexDirection: 'row' }]}>
              <View style={[{ width: 145 }]}>
                <Text >{`Sử dụng tiền thưởng`}</Text>
                <Text style={{ fontWeight: 'bold', fontSize: 16, color: 'red' }}>{`(${userInfo.money === undefined ? 0 : userInfo.money} VNĐ)`}</Text>
              </View>
              <View
                ref={el => this.positionControls["price"] = el}
                style={{ borderWidth: 1, flex: 1, borderColor: this.state.isErrorPrice === true ? 'red' : 'grey', paddingLeft: 20, borderRadius: 30 }}
              >
                <TextInput
                  name={'price'}
                  placeholder={`${userInfo.money === undefined ? 0 : userInfo.money}`}
                  onChangeText={(text) => this.onChangeTextMoneyFee(text)}
                  multiline={true}
                  selectTextOnFocus={true}
                  keyboardType={'numeric'}
                />
              </View>
            </View>
            {
              this.state.isErrorPrice ? (
                <Text style={{ color: 'red' }}>{"Bạn đang sử dụng quá tiền thưởng!"}</Text>
              ) : null
            }
            {
              <Text>{'Bạn có thể sử dụng tiền thưởng để thanh toán!'}</Text>
            }
          </View>
        </View>
      </ScrollView>
    )
  }
}

export { OrderStep3 };

