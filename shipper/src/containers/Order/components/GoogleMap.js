import React, { Component } from 'react';
import { StyleSheet, View, Image } from 'react-native';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';

const GOOGLE_MAPS_APIKEY = "AIzaSyCx3c8GVP57CDXvA8l1iCNRlMFrXdj7G_0";

const styles = StyleSheet.create({
  containerMap: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },

  map: {
    ...StyleSheet.absoluteFillObject,
    flex: 1
  },

  container: {
    justifyContent: "center",
    alignItems: "center",
    flex: 1
  },

  containerControl: {
    position: 'absolute',
    bottom: 0,
    alignSelf: 'center',
    flex: 1
  },

  containerBack: {
    position: 'absolute',
    top: 50,
    left: 20,
    backgroundColor: "rgba(255, 255, 255, 0.5)",
    padding: 16,
    borderRadius: 16
  },

  btn: {
    padding: 16,
    borderRadius: 8,
    backgroundColor: "#fff",
    textAlign: "center",
  },
})

class GoogleMap extends Component {
  constructor(props) {
    super(props)
    this.state = this._getInitialState()
  }

  async componentDidMount() {
    await this._initializeAsync()
  }

  _initializeAsync = async () => {
    const { origin, destination, address } = this.props;

    await this._setStateAsync({
      address,
      originMarker: {
        ...this.state.originMarker,
        latitude: origin.lat,
        longitude: origin.lng,
      },
      destinationMarker: {
        ...this.state.destinationMarker,
        latitude: destination.lat,
        longitude: destination.lng
      },
      isLoading: false
    })
  }

  _getInitialState = () => {
    return {
      isLoading: true,
      currentRegion: null,
      address: "",
      originMarker: {
        latitude: undefined,
        longitude: undefined,
        latitudeDelta: 0.05,
        longitudeDelta: 0.05,
      },

      destinationMarker: {
        latitude: undefined,
        longitude: undefined,
        latitudeDelta: 0.05,
        longitudeDelta: 0.05,
      },

      currentLocation: {
        latitude: 0,
        longitude: 0,
        latitudeDelta: 0.02,
        longitudeDelta: 0.02,
      },

      region: {
        latitude: 10.9529564,
        longitude: 106.8025003,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
      },
    };
  }

  _renderDestinationMarker = () => {
    const { destinationMarker, address } = this.state

    return (
      <MapView.Marker
        coordinate={{
          latitude: destinationMarker.latitude,
          longitude: destinationMarker.longitude
        }}
        title={"Điểm nhận"}
        description={address.destination || ""}
      >
        <Image
          resizeMode="stretch"
          style={{ width: 40, height: 50 }}
          source={require('../../../images/icon-marker-to.png')}
        />
      </MapView.Marker>
    )
  }

  _renderDrawStroke = () => {
    const { originMarker, destinationMarker } = this.state;

    return (
      <MapViewDirections
        apikey={GOOGLE_MAPS_APIKEY}
        strokeWidth={3}
        strokeColor="red"
        origin={{
          longitude: originMarker.longitude,
          latitude: originMarker.latitude
        }}
        destination={{
          latitude: destinationMarker.latitude,
          longitude: destinationMarker.longitude
        }}
      />
    )
  }

  render() {
    const { originMarker, isLoading, address } = this.state

    return (
      <View style={styles.containerMap}>
        {
          !isLoading &&
          <MapView
            provider={PROVIDER_GOOGLE}
            style={styles.map}
            initialRegion={originMarker}
          >
            <MapView.Marker
              coordinate={{
                latitude: originMarker.latitude,
                longitude: originMarker.longitude
              }}
              title={"Điểm giao"}
              description={address.origin || ""}
            >
              <Image
                resizeMode="stretch"
                style={{ width: 40, height: 50 }}
                source={require('../../../images/icon-marker-from.png')}
              />
            </MapView.Marker>
            {this._renderDestinationMarker()}
            {this._renderDrawStroke()}
          </MapView> || null
        }
      </View>
    );
  }
}

export { GoogleMap };

