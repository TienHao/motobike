// libary
import React, { PureComponent } from 'react';
import { ScrollView, Text, View, Image } from 'react-native';
import { styles } from "../style";

// utils
import { currencyFormat } from 'utils';

class OrderStep4 extends PureComponent {
  constructor(props) {
    super(props);

    const { serviceText } = this.props.getDataByStep(2);
    this.validForm = [];
    this.positionControls = [];

    this.state = {
      ...this.getInitialState(),
      ...this.props.getDataByStep(4),
      ...this.props.getDataByStep(3),
      serviceText
    };
  }

  getInitialState = () => {
    return {}
  }

  checkValidate = async () => {

    try {
      return {
        valid: true,
        data: {
          step1: { ...this.props.getDataByStep(1) },
          step2: { ...this.props.getDataByStep(2) },
          step3: { ...this.props.getDataByStep(3) }
        }
      }
    } catch (error) {
      return false;
    }
  }

  render() {
    const { responseData } = this.state;
    const step1 = responseData[0];
    const step2 = responseData[1];
    const step3 = this.state.result;

    const { userInfo, money_bonus } = this.props;

    const money = userInfo.money === undefined ? 0 : userInfo.money;

    const priceDeliver = step3.fee > money ? step3.fee - money : money - step3.fee;

    const sumPrice = priceDeliver * 1000 - money_bonus;

    console.log('data', step2);

    return (
      <ScrollView >
        <View style={styles.containerForm}>
          <View>
            <Text style={[styles.titleBlock, { marginBottom: 10 }]}>
              Người giao
            </Text>

            <View style={[styles.row, styles.line]}>
              <View style={styles.col}>
                <Text>Họ tên</Text>
              </View>
              <View style={styles.col}>
                <Text>{step1.sender_fullname}</Text>
              </View>
            </View>

            <View style={[styles.row, styles.line]}>
              <View style={styles.col}>
                <Text>Số điện thoại</Text>
              </View>
              <View style={styles.col}>
                <Text>{step1.sender_phone}</Text>
              </View>
            </View>

            <View style={[styles.row, styles.line]}>
              <View style={styles.col}>
                <Text>Địa chỉ</Text>
              </View>
              <View style={styles.col}>
                <Text>{step1.address_to && step1.address_from}</Text>
              </View>
            </View>
          </View>

          <View>
            <Text style={[styles.titleBlock, { marginBottom: 10 }]}>
              Người Nhận
            </Text>

            <View style={[styles.row, styles.line]}>
              <View style={styles.col}>
                <Text>Họ tên</Text>
              </View>
              <View style={styles.col}>
                <Text>{step1.receiver_fullname}</Text>
              </View>
            </View>

            <View style={[styles.row, styles.line]}>
              <View style={styles.col}>
                <Text>Số điện thoại</Text>
              </View>
              <View style={styles.col}>
                <Text>{step1.receiver_phone}</Text>
              </View>
            </View>

            <View style={[styles.row, styles.line]}>
              <View style={styles.col}>
                <Text>Địa chỉ</Text>
              </View>
              <View style={styles.col}>
                <Text>{step1.address_from && step1.address_from}</Text>
              </View>
            </View>
          </View>

          <View>
            <Text style={[styles.titleBlock, { marginBottom: 10 }]}>
              Thông tin hàng
            </Text>

            <View style={[styles.row, styles.line]}>
              <View style={styles.col}>
                <Text>Tên hàng</Text>
              </View>
              <View style={styles.col}>
                <Text>{step2.product_infor}</Text>
              </View>
            </View>

            <View style={[styles.row, styles.line]}>
              <View style={styles.col}>
                <Text>Khối lượng</Text>
              </View>
              <View style={styles.col}>
                <Text>{step2.mass} kg</Text>
              </View>
            </View>

            <View style={{ flexDirection: 'row' }}>
              <View style={{ flex: 5 }}>
                <Text style={{ fontSize: 14 }}>Tiền vận chuyển do</Text>
              </View>
              <View style={{ flex: 5, justifyContent: 'center', alignItems: 'flex-start', marginBottom: 10 }}>
                <Text>{step2.payfee == 0 ? "Người giao trả" : "Người nhận trả"}</Text>
              </View>
            </View>

            <View style={[styles.line]}>
              <View style={{ flex: 1, marginBottom: 10 }}>
                <Text>Ảnh chụp hàng</Text>
              </View>
              <View style={{ flex: 1 }}>
                <Image
                  style={{ height: 200 }}
                  source={{ uri: step2.image }}
                />
              </View>
            </View>

          </View>

          <View>
            <Text style={[styles.titleBlock, { marginBottom: 10 }]}>
              Cước phí
            </Text>

            <View style={[{ flexDirection: 'column' }]}>
              <View style={[styles.line, { flexDirection: 'row' }]}>
                <View style={{ flex: 5 }}>
                  <Text>{"Cước phí hiện tại: "}</Text>
                </View>
                <View style={{ justifyContent: 'center', alignItems: 'center', flex: 5 }}>
                  <Text>{currencyFormat(priceDeliver)} VNĐ</Text>
                </View>
              </View>
              <View style={[styles.line, { flexDirection: 'row' }]}>
                <View style={{ flex: 5 }}>
                  <Text>{"Sử dụng tiền thưởng: "}</Text>
                </View>
                <View style={{ justifyContent: 'center', alignItems: 'center', flex: 5 }}>
                  <Text>{this.props.money_bonus} VNĐ</Text>
                </View>
              </View>
              <View style={[styles.line, { flexDirection: 'row' }]}>
                <View style={{ flex: 5 }}>
                  <Text style={{ color: 'red', fontWeight: 'bold', fontSize: 18 }}>{"Tổng Tiền: "}</Text>
                </View>
                <View style={{ flex: 5, justifyContent: 'center', alignItems: 'center' }}>
                  <Text style={{ color: 'red', fontWeight: 'bold', fontSize: 17 }}>{sumPrice} VNĐ</Text>
                </View>
              </View>
            </View>
          </View>
          <View style={{ paddingVertical: 8 }}></View>
        </View>
      </ScrollView>
    )
  }
}

export { OrderStep4 };

