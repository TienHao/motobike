// libary
import React, { Component } from 'react';
import { StatusBar, View } from 'react-native';
import { NavigationEvents } from 'react-navigation';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

// components
import MapSearchLocation from '../MapSearchLocation';

class MapScreen extends Component {
  constructor(props) {
    super(props);


    this.state = this.getInitialState();
    this.initialize();
  }

  static navigationOptions = ({ navigation }) => {
    return {
      title: "Chọn địa chỉ"
    }
  }

  getInitialState = () => {
    return {}
  }

  initialize = () => {
    StatusBar.setBarStyle('dark-content');
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <NavigationEvents
          onWillFocus={payload => this.initialize()}
        />
        <MapSearchLocation {...this.props} />
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    valueCheckPeople: state.mapReducer.valueCheckPeople
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}

const MapContainer = connect(mapStateToProps, mapDispatchToProps)(MapScreen);
export { MapContainer };

