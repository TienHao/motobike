import { colors } from "config/colors";
import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.bgBody,
  },
  containerSeparate: {
    justifyContent: "center",
    flexDirection: "row",
    alignItems: "center",
    marginHorizontal: 5
  },
  containerInput: {
    marginBottom: 0
  },

  btn: {
    flex: 1,
    backgroundColor: colors.primary.default,
    marginTop: 15,
    marginBottom: 5,
    height: 40,
    overflow: "hidden",
    justifyContent: 'center',
  },

  btnText: {
    textAlign: "center",
    color: "#fff",
    fontSize: 16,
  }
})
