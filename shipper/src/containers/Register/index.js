// // libary
// import { addUserInfoAction } from 'actions';
// import { callApi, getApiLogin, getApiRegister } from "../../api";
// import React, { Component } from 'react';
// import { StatusBar, Text, TouchableOpacity, TouchableWithoutFeedback, View } from 'react-native';
// import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
// import { connect } from 'react-redux';
// import { styles } from './styles';
// import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

// // components
// import RegisterSocial from './components/RegisterSocial';
// import OfoTextInput from '../../components/OfoTextInput';
// import RadioButton from '../../components/RadioButton';

// // utils
// import { myRule } from '../../utilities';


// class RegisterScreen extends Component {
//   constructor(props) {
//     super(props);

//     this.state = {
//       email: {
//         value: '',
//         isValidating: null,
//         errorMessage: "Bạn nhập email chưa đúng."
//       },
//       phone: {
//         value: '',
//         isValidating: null,
//         errorMessage: "Bạn nhập số điện thoại chưa hợp lệ."
//       },
//       password: {
//         value: '',
//         isValidating: null
//       },
//       confirmPassword: {
//         value: '',
//         isValidating: null
//       },
//       fullName: {
//         value: '',
//         isValidating: null
//       },
//       errorEmailMessage: '',
//       radioItems: [
//         {
//           label: "Shipper",
//           size: 25,
//           color: 'green',
//           isSelected: false,
//           type_id: 0
//         },
//         {
//           label: "Customer",
//           size: 25,
//           color: 'green',
//           isSelected: true,
//           type_id: 1
//         }
//       ],
//       selectedItem: ''
//     }
//   }

//   componentDidMount() {
//     this._navListenerFocus = this.props.navigation.addListener('willFocus', () => {
//       StatusBar.setHidden(true);
//     });

//     this._navListenerBlur = this.props.navigation.addListener('willBlur', () => {
//       StatusBar.setHidden(false);
//     });

//     this.state.radioItems.map((item) => {
//       if (item.isSelected == true) {
//         this.setState({
//           selectedItem: item.label
//         })
//       }
//     })
//   }

//   componentWillUnmount() {
//     this._navListenerFocus.remove();
//     this._navListenerBlur.remove();
//   }

//   onCickChangeActiveRadioButton = (index) => {
//     this.state.radioItems.map((item) => {
//       item.isSelected = false;
//     });

//     this.state.radioItems[index].isSelected = true;

//     this.setState({
//       radioItems: this.state.radioItems
//     }, () => {
//       this.setState({ selectedItem: this.state.radioItems[index].label })
//     })
//   }

//   onChangeText = (name, value) => {
//     let ruleCheckEmail = myRule.email(value);
//     let ruleCheckPhone = myRule.phoneNumber(value);

//     if (name === 'email') {
//       this.setState({
//         [name]: {
//           ...this.state[name],
//           value: value,
//           isValidating: ruleCheckEmail
//         }
//       });
//     } else if (name === 'phone') {
//       this.setState({
//         [name]: {
//           ...this.state[name],
//           value: value,
//           isValidating: ruleCheckPhone
//         }
//       })
//     } else {
//       this.setState({
//         [name]: {
//           ...this.state[name],
//           value: value,
//           isValidating: true
//         }
//       })
//     }
//   }

//   onEndEditingCheckError = (name, value) => {
//     if (this.state[name].value === '') {
//       this.setState({
//         [name]: {
//           ...this.state[name],
//           isValidating: false
//         }
//       })
//     }
//   }

//   checkErrorForm = () => {
//     const names = ["fullName", "email", "phone", "password", "confirmPassword"];

//     names.forEach(name => {
//       if (this.state[name].value === '') {
//         this.setState({
//           [name]: {
//             ...this.state[name],
//             isValidating: false
//           }
//         });
//       }
//     })

//     return {
//       data: {
//         "fullName": this.state.fullName.value,
//         "email": this.state.email.value,
//         "phone": this.state.phone.value,
//         "password": this.state.password.value,
//         "confirm": this.state.confirmPassword.value
//       }
//     }
//   }

//   checkDataComplete = async () => {
//     const { data } = await this.checkErrorForm();
//     const names = ["fullName", "email", "phone", "password", "confirmPassword"];

//     const checkPushData = names.every(name => {
//       return data[name] !== '';
//     })

//     return {
//       data,
//       checkPushData
//     };
//   }

//   goLoginApp = async () => {
//     const authorization = this.props.accessToken;

//     const { email, password } = this.state;
//     const api = getApiLogin({ authorization, data: { email: email.value, password: password.value } });
//     const res = await callApi(api);
//     if (res.success) {
//       this.props.addUserInfoAction({ userInfo: res.data.user_info });
//       this.props.navigation.popToTop();
//     }
//   }

//   onPressRegister = async () => {
//     const { data, checkPushData } = await this.checkDataComplete();

//     if (checkPushData) {
//       const res = await callApi(getApiRegister({ data }));

//       if (res && res.success) {
//         this.setState({
//           errorEmailMessage: ""
//         })
//         this.goLoginApp();
//       } else {
//         this.setState({ errorEmailMessage: res.message.error_message })
//       }
//     }
//   }

//   render() {
//     const { email, password, fullName, confirmPassword, phone } = this.state;
//     return (
//       <View style={styles.container}>
//         <View style={{
//           right: 0,
//           top: 0,
//           elevation: 5,
//           flexDirection: 'row',
//           shadowOpacity: 5,
//           padding: 10,
//           backgroundColor: 'white'
//         }}>
//           <TouchableWithoutFeedback
//             style={{ width: 50, height: 50 }}
//             onPress={() => this.props.navigation.popToTop()}
//           >
//             <MaterialCommunityIcons name="arrow-left" style={{ fontSize: 24 }} />
//           </TouchableWithoutFeedback>
//           <View style={{ justifyContent: 'center', alignItems: 'center', flex: 6 }}>
//             <Text style={{ fontSize: 16 }}>Đăng ký</Text>
//           </View>
//         </View>
//         <KeyboardAwareScrollView
//           style={{ paddingHorizontal: 16 }}
//           enableOnAndroid={true}
//         >
//           <View
//             style={styles.containerInput}
//           >
//             <OfoTextInput
//               placeholder="Nhập tên"
//               label="Nhập tên *"
//               name="fullName"
//               value={this.state.fullName.value}
//               onChangeText={this.onChangeText}
//               onEndEditingCheckError={this.onEndEditingCheckError}
//               error={fullName.isValidating !== null ? !fullName.isValidating : null}
//               success={fullName.isValidating !== null ? fullName.isValidating : null}
//             />
//           </View>

//           <View
//             style={styles.containerInput}
//           >
//             <OfoTextInput
//               placeholder="Nhập email"
//               label="Nhập email (tác dụng phụ để khôi phục mật khẩu)"
//               name="email"
//               value={email.value}
//               onChangeText={this.onChangeText}
//               errorMessage={this.state.email.errorMessage}
//               onEndEditingCheckError={this.onEndEditingCheckError}
//               error={email.isValidating !== null ? !email.isValidating : null}
//               success={email.isValidating !== null ? email.isValidating : null}
//             />
//           </View>

//           <View
//             style={styles.containerInput}
//           >
//             <OfoTextInput
//               placeholder="Nhập Số điện thoại"
//               label="Nhập số điện thoại *"
//               type="phoneNumber"
//               name="phone"
//               value={phone.value}
//               onChangeText={this.onChangeText}
//               errorMessage={this.state.phone.errorMessage}
//               onBlurCheckTextError={this.onBlurCheckTextError}
//               onEndEditingCheckError={this.onEndEditingCheckError}
//               error={phone.isValidating !== null ? !phone.isValidating : null}
//               success={phone.isValidating !== null ? phone.isValidating : null}
//             />
//           </View>

//           <View
//             style={styles.containerInput}
//           >
//             <OfoTextInput
//               placeholder="Nhập mật khẩu"
//               type="password"
//               label="Nhập mật khẩu *"
//               name="password"
//               value={password.value}
//               onChangeText={this.onChangeText}
//               onBlurCheckTextError={this.onBlurCheckTextError}
//               onEndEditingCheckError={this.onEndEditingCheckError}
//               error={password.isValidating !== null ? !password.isValidating : null}
//               success={password.isValidating !== null ? password.isValidating : null}
//             />
//           </View>

//           <View
//             style={styles.containerInput}
//           >
//             <OfoTextInput
//               placeholder="Nhập lại mật khẩu"
//               type="password"
//               label="Nhập lại mật khẩu *"
//               name="confirmPassword"
//               value={confirmPassword.value}
//               onChangeText={this.onChangeText}
//               onBlurCheckTextError={this.onBlurCheckTextError}
//               onEndEditingCheckError={this.onEndEditingCheckError}
//               error={confirmPassword.isValidating !== null ? !confirmPassword.isValidating : null}
//               success={confirmPassword.isValidating !== null ? confirmPassword.isValidating : null}
//             />
//           </View>

//           <View style={styles.containerInput}>
//             <TouchableOpacity
//               onPress={() => this.onPressRegister()}
//             >
//               <View style={styles.btn}>
//                 <Text style={styles.btnText}>Đăng ký</Text>
//               </View>
//             </TouchableOpacity>
//           </View>

//           <View style={styles.containerSeparate}>
//             <View style={{ flex: 2, borderBottomWidth: 1, borderBottomColor: "#E6E6E6" }}>

//             </View>
//             <View style={{ flex: 1 }}>
//               <Text style={{ textAlign: "center", color: "#B8B8B8" }}>Hoặc</Text>
//             </View>
//             <View style={{ flex: 2, borderBottomWidth: 1, borderBottomColor: "#E6E6E6" }}>

//             </View>
//           </View>

//           <RegisterSocial
//             {...this.props}
//           />

//         </KeyboardAwareScrollView>
//       </View>
//     )
//   }
// }

// function mapStateToProps(state) {
//   return {
//     accessToken: state.authReducer.accessToken
//   }
// }

// const mapDispatchToProps = {
//   addUserInfoAction
// }

// const RegisterContainer = connect(mapStateToProps, mapDispatchToProps)(RegisterScreen);
// export { RegisterContainer };


// libary
import { addUserInfoAction } from 'actions';
import { callApi, getApiLogin, getApiRegister, getDeviceId } from "api";
import { MyInputText } from 'controls';
import React, { Component } from 'react';
import { findNodeHandle, StatusBar, Text, TouchableOpacity, TouchableWithoutFeedback, View } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Icon } from 'react-native-ui-kitten';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { myRule } from "utils";
import { styles } from './styles';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import DeviceInfo from 'react-native-device-info';

// components
import RegisterSocial from './components/RegisterSocial';

class RegisterScreen extends Component {
  constructor(props) {
    super(props);

    this.state = this._getInitialState();
    this._validForm = [];
    this._positionControls = [];
  }

  componentDidMount() {
    this._navListenerFocus = this.props.navigation.addListener('willFocus', () => {
      StatusBar.setHidden(true);
    });

    this._navListenerBlur = this.props.navigation.addListener('willBlur', () => {
      StatusBar.setHidden(false);
    });
  }

  componentWillUnmount() {
    this._navListenerFocus.remove();
    this._navListenerBlur.remove();
  }

  _getInitialState = () => {
    return {
      isCheckValid: false,
      emailErrorMessage: "",
      fullName: "",
      email: "",
      phone: "",
      password: "",
      confirm: "",
    }
  }

  _scrollTo = ({ x, y }) => {
    this._scrollView.scrollTo({ x: 0, y, animated: true })
  }

  _calculatePosition = (name) => {
    this._positionControls[name].measureLayout(
      findNodeHandle(this._scrollView),
      (x, y) => {
        this._scrollTo({ x, y })
      }
    )
  }

  _checkValidate = async () => {
    try {
      this._validForm = []
      await this._setStateAsync({ isCheckValid: true });
      this.forceUpdate()

      return {
        valid: this._validForm.every(control => {
          if (!control.valid) {
            this._calculatePosition(control.name);
            return false
          }
          return true
        }),
        data: this.state
      }

    } catch (error) {
      return false;
    }
  }

  _login = async () => {
    const authorization = this.props.accessToken;
    const { email, password } = this.state;
    const api = getApiLogin({ authorization, data: { email, password } });
    const res = await callApi(api);
    if (res.success) {
      this.props.addUserInfoAction({ userInfo: res.data.user_info });
      this.props.navigation.popToTop();
    }
  }

  _register = async () => {
    const { valid, data } = await this._checkValidate();
    let deviceId = DeviceInfo.getUniqueId();

    if (valid) {
      const res = await callApi(getApiRegister({ data }));
      const resInfo = await callApi(getDeviceId({ authorization, data: { "member_id": 1, "device_id": deviceId } }));

      if (res && res.success) {
        this.setState({ emailErrorMessage: "" });
        this._login();

      } else { // Email was registered
        this.setState({ emailErrorMessage: res.message.error_message })
      }
    }
  }

  _handleChange = ({ name, value }) => {
    this.setState({ [name]: value, emailErrorMessage: "" });
  }

  render() {
    const { isCheckValid, password } = this.state

    return (
      <View style={styles.container}>
        <View style={{
          right: 0,
          top: 0,
          elevation: 5,
          flexDirection: 'row',
          shadowOpacity: 5,
          padding: 10,
          backgroundColor: 'white',
          marginBottom: 10
        }}>
          <TouchableWithoutFeedback
            style={{ width: 50, height: 50 }}
            onPress={() => this.props.navigation.popToTop()}
          >
            <MaterialCommunityIcons name="arrow-left" style={{ fontSize: 24 }} />
          </TouchableWithoutFeedback>
          <View style={{ justifyContent: 'center', alignItems: 'center', flex: 6 }}>
            <Text style={{ fontSize: 16 }}>Đăng ký</Text>
          </View>
        </View>
        <KeyboardAwareScrollView
          style={{ paddingHorizontal: 16 }}
          enableOnAndroid={true}
          innerRef={ref => {
            this._scrollView = ref
          }}
        >
          <View
            style={styles.containerInput}
            ref={el => this._positionControls["fullName"] = el}
          >
            <MyInputText
              rules={[myRule.required]}
              checkValidForm={isCheckValid}
              label={"Họ tên *"}
              placeholder={"Nhập họ tên"}
              name={"fullName"}
              value={this.state.fullName}
              onChangeText={this._handleChange}
              onValidate={(valid) => this._validForm.push(valid)}
            />
          </View>

          <View
            style={styles.containerInput}
            ref={el => this._positionControls["email"] = el}
          >
            <MyInputText
              rules={[myRule.required, myRule.email]}
              checkValidForm={isCheckValid}
              label={"Email *"}
              placeholder={"Nhập email"}
              name={"email"}
              value={this.state.email}
              onChangeText={this._handleChange}
              onValidate={(valid) => this._validForm.push(valid)}
              error={this.state.emailErrorMessage}
            />
          </View>

          <View
            style={styles.containerInput}
            ref={el => this._positionControls["phone"] = el}
          >
            <MyInputText
              rules={[myRule.required, myRule.phoneNumber]}
              checkValidForm={isCheckValid}
              label={"Điện thoại *"}
              name={"phone"}
              placeholder={"Nhập số điện thoại"}
              value={this.state.phone}
              onChangeText={this._handleChange}
              onValidate={(valid) => this._validForm.push(valid)}
              keyboardType="phone-pad"
              error={this.state.emailErrorMessage}
            />
          </View>

          <View
            style={styles.containerInput}
            ref={el => this._positionControls["password"] = el}
          >
            <MyInputText
              rules={[myRule.required, myRule.minLength.bind(null, [8])]}
              type="password"
              checkValidForm={isCheckValid}
              label={"Mật khẩu *"}
              placeholder={"Nhập mật khẩu"}
              name={"password"}
              value={this.state.password}
              onChangeText={this._handleChange}
              onValidate={(valid) => this._validForm.push(valid)}
            />
          </View>

          <View
            style={styles.containerInput}
            ref={el => this._positionControls["confirm"] = el}
          >
            <MyInputText
              type="password"
              rules={[myRule.required, myRule.minLength.bind(null, [8]), myRule.equal.bind(null, [password, 'Mật khẩu không trùng khớp'])]}
              checkValidForm={isCheckValid}
              label={"Xác nhận mật khẩu *"}
              placeholder={"Nhập lại mật khẩu"}
              name={"confirm"}
              value={this.state.confirm}
              onChangeText={this._handleChange}
              onValidate={(valid) => this._validForm.push(valid)}
            />
          </View>

          <View style={styles.containerInput}>
            <TouchableOpacity
              onPress={() => this._register()}
            >
              <View style={styles.btn}>
                <Text style={styles.btnText}>Đăng ký</Text>
              </View>
            </TouchableOpacity>
          </View>

          <View style={styles.containerSeparate}>
            <View style={{ flex: 2, borderBottomWidth: 1, borderBottomColor: "#E6E6E6" }}>

            </View>
            <View style={{ flex: 1 }}>
              <Text style={{ textAlign: "center", color: "#B8B8B8" }}>Hoặc</Text>
            </View>
            <View style={{ flex: 2, borderBottomWidth: 1, borderBottomColor: "#E6E6E6" }}>

            </View>
          </View>

          <RegisterSocial
            {...this.props}
          />
        </KeyboardAwareScrollView>
      </View >
    )
  }
}

function mapStateToProps(state) {
  const { accessToken } = state.authReducer;
  return { accessToken };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ addUserInfoAction }, dispatch);
}

const RegisterContainer = connect(mapStateToProps, mapDispatchToProps)(RegisterScreen);
export { RegisterContainer };


