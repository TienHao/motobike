import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: "row",
      backgroundColor: '#3F51B5',
    },    
    containerIcon: {
      justifyContent: 'flex-end',
      alignItems: 'center',
      flexDirection: 'row',
      marginLeft: 25
    },
    containerCaption: {
      flex: 0.8,
      alignItems: 'center',
      justifyContent: 'center'
    }
  });