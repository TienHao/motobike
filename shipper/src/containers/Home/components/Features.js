import React from 'react';
import { StyleSheet, Text, TouchableWithoutFeedback, View } from 'react-native';
import { Icon } from 'react-native-ui-kitten';

const styles = StyleSheet.create({
  containerItem: {
    flex: 1,
    height: 150,
    backgroundColor: "#fff",
    borderRadius: 16,
    marginHorizontal: 8
  },
  shadowStyle: {
    shadowColor: "#000",
    shadowOffset: {
      width: 10,
      height: 20,
    },
    shadowOpacity: 0.3,
    shadowRadius: 3,
    elevation: 5,
  },
  border: {
    borderWidth: 0.5,
    borderColor: "#f0f0f0"
  }
})

export const Features = function (props) {

  return (
    <View style={{ flexDirection: "row" }}>
      <TouchableWithoutFeedback onPress={() => props.navigation.navigate('Order')}>
        <View style={[styles.containerItem, styles.border]}>
          <View style={{ flexDirection: "column", flex: 1, justifyContent: "center" }}>
            <View style={{ flex: 3, justifyContent: "center", alignItems: "center" }}>
              <Icon name="file-text" width={64} height={64} fill={"#E94B3C"} />
            </View>
            <View style={{ flex: 1, justifyContent: "flex-start" }}>
              <Text style={{ textAlign: "center", fontSize: 16 }}>Tạo đơn hàng</Text>
            </View>
          </View>
        </View>
      </TouchableWithoutFeedback>

      <TouchableWithoutFeedback onPress={() => props.navigation.navigate('History')}>
        <View style={[styles.containerItem, styles.border]}>
          <View style={{ flexDirection: "column", flex: 1, justifyContent: "center" }}>
            <View style={{ flex: 3, justifyContent: "center", alignItems: "center" }}>
              <Icon name="clock" width={64} height={64} fill={"#F96714"} />
            </View>
            <View style={{ flex: 1, justifyContent: "flex-start" }}>
              <Text style={{ textAlign: "center", fontSize: 16 }}>Xem lịch sử</Text>
            </View>
          </View>
        </View>
      </TouchableWithoutFeedback>
    </View>
  )
}
