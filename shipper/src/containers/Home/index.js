
// libary
import { HeaderComp } from 'components/Header';
import React from 'react';
import { StatusBar, View, TouchableOpacity, Clipboard, Alert } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import firebase from 'react-native-firebase';
import { RemoteMessage, Notification } from 'react-native-firebase';
import PushNotification from 'react-native-push-notification';

// components
import OrderInProgress from '../OrderInProgress';
import OrderComplete from '../OrderComplete';
import CancelOrder from '../CancelOrder';

// constanst
import {
  SHIPPER,
  RENDER_PAGE_CANCEL_ORDER,
  RENDER_PAGE_IN_PROGRESS_ORDER,
  RENDER_PAGE_COMPLETE_ORDER,
  SHIPPER_OUT_OF_MONEY_PUSH_NOTI
} from '../../constanst';

// actions
import { changeStatusPushNotification } from '../../actions';

import { styles } from './style';

const notification = new firebase.notifications.Notification();

class HomeScreen extends React.Component {
  constructor(props) {
    super(props)


    this.state = this._getInitialState();
  }

  _getInitialState = () => {
    return {
      valueCheckPageRender: RENDER_PAGE_COMPLETE_ORDER,
      valueCheckPushNoti: 0
    }
  }

  async componentDidMount() {
    const { navigation, userInfo } = this.props;

    const moneyShipper = parseInt(userInfo.deposit);

    if (moneyShipper < 0 && userInfo.type_id == SHIPPER) {
      this.props.changeStatusPushNotification({ valueCheckPushNoti: 4 }, SHIPPER_OUT_OF_MONEY_PUSH_NOTI);
    }

    this.focusListener = navigation.addListener('willFocus', () => {
      StatusBar.setTranslucent(true);
      StatusBar.setBarStyle('light-content');

      this.forceUpdate();
    });

    this.notificationDisplayedListener = firebase.notifications().onNotificationDisplayed((notification) => {
      console.log('notification', notification);
    });
    this.notificationListener = firebase.notifications().onNotification((notification) => {
      let message = notification.title;
      let title = 'Motobike';
      this.showNotification(message, title);
    });
  }

  showNotification = (message, title) => {
    PushNotification.localNotification({
      message: message,
      bigText: message,
      subText: message,
      title: title,
      actions: '["Chi Tiết", "Hủy bỏ"]'
    })
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.valueCheckPushNoti !== prevProps.valueCheckPushNoti) {
      this.messageListener();
      this.forceUpdate();
    }
  }

  messageListener() {
    firebase.messaging().onMessage(function (payload) {
      console.log('paload ', payload);
    });
  }

  componentWillUnmount() {
    this.messageListener();
    this.notificationListener();
    this.notificationDisplayedListener();
    this.focusListener.remove();
  }

  checkPageRender = (valueCheck) => {
    this.setState({
      valueCheckPageRender: valueCheck
    })
  }

  onpressCreateOrder = () => {
    this.props.navigation.navigate("Order");
  }

  render() {
    const { userInfo } = this.props;

    return (
      <View style={styles.container}>
        <HeaderComp
          callbackCheckPageRender={this.checkPageRender}
          {...this.props} />
        {
          this.state.valueCheckPageRender === RENDER_PAGE_IN_PROGRESS_ORDER ?
            (
              <OrderInProgress
                accessToken={this.props.accessToken}
                userInfo={userInfo}
                {...this.props} />
            ) : this.state.valueCheckPageRender === RENDER_PAGE_COMPLETE_ORDER ? (
              <OrderComplete {...this.props} />
            ) : this.state.valueCheckPageRender === RENDER_PAGE_CANCEL_ORDER ? (
              <CancelOrder {...this.props} />
            ) : null
        }

        {userInfo.type_id == SHIPPER ? (
          null
        ) : (
            <TouchableOpacity
              activeOpacity={0.7}
              onPress={this.onpressCreateOrder}
              style={styles.buttonFLoatingContainer}
            >
              <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                <IconFontAwesome name='cart-plus' style={[{ fontSize: 25, color: 'white' }]} />
              </View>
            </TouchableOpacity>
          )}
      </View>
    );
  }
}

function mapStateToProps(state, ownProps) {
  const { accessToken, userInfo } = state.authReducer;

  return {
    accessToken,
    userInfo,
    valueCheckPushNoti: state.notiReducer.valueCheckPushNoti,
    addressDeliver: state.mapReducer.addressDeliver,
    addressReceiver: state.mapReducer.addressReceiver,
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ changeStatusPushNotification }, dispatch);
}

const HomeContainer = connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
export { HomeContainer };

