import { base } from 'api';
import React from 'react';
import { View } from 'react-native';
import { WebView } from 'react-native-webview';
import { getHeightPercentage } from "utils";

export const ArticleDetail = function(props) {

  return (
    <View style={{flex: 1}}>
      <WebView
        scalesPageToFit={false}
        bounces={false}
        scrollEnabled={false}
        // injectedJavaScript={`<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0">`}
        style={{ height: getHeightPercentage(100) }}
        originWhitelist={['*']}
        source={{html:
        `<!DOCTYPE html>
          <html>
            <head>
              <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0">
              <style>
              body {
                margin: 0;
                padding: 0;
              }
              .cover {
                object-fit: cover;
                width: 100%;
                height: 200px;
              }
              </style>
            </head>
            <body>
              <div>
                <div>
                  <img class="cover" src="${base}/${props.image}" />
                </div>

                <div style="text-align: justify; padding: 0px 16px; margin-bottom: 32px;">
                  <div>
                    <h3>${props.title}</h3>
                  </div>
                  <div>
                    ${props.Content}
                  </div>
                </div>
              </div>
            </body>
          </html>
        `
        }}
      />
    </View>
  )
}
