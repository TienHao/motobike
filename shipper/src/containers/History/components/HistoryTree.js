// libary
import { callApi, getApiHistoryTree } from 'api';
import { Loading } from 'components/Loading';
import React, { Fragment, PureComponent } from 'react';
import { ScrollView, Text, View } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { styles } from '../style';
import { GoogleMap } from './GoogleMap';

class HistoryTreeScreen extends PureComponent {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Theo dõi vận đơn'
    };
  };


  constructor(props) {
    super(props);

    const { navigation } = this.props;
    this._historyCode = navigation.getParam('history', 'some default value');
    this.state = this._getInitialState();
    this._initializeAsync();
  }

  _initializeAsync = async () => {
    const authorization = this.props.accessToken
    const res = await callApi(getApiHistoryTree({ authorization, code: this._historyCode }))

    if (res.success) {
      await this._setStateAsync({
        address: res.data.full_address,
        location: res.data.location,
        treeHistory: res.data.status_update_detail,
        isLoading: false
      })
    }
  }

  _getInitialState = () => ({
    isLoading: true,
    treeHistory: [],
    location: "",
    address: ""
  })

  _renderItem = () => {
    const { treeHistory } = this.state
    if (!treeHistory.length) {
      return (
        <View style={{ flex: 1, justifyContent: "center" }}>
          <Text>Mã vận đơn không đúng</Text>
        </View>
      )
    }

    return (
      treeHistory.map((item, index) => (
        <View style={{ flexDirection: "row" }} key={item.id}>
          <View style={{ flexDirection: "column", justifyContent: "center", alignItems: "center" }}>
            <View style={{ borderRadius: 10, borderWidth: 3, height: 20, width: 20, borderColor: "#212C6C" }}></View>
            <View style={{ borderLeftWidth: 2, borderLeftColor: "#D9D9D9", height: 50 }}></View>
          </View>
          <View style={{ flex: 1, paddingLeft: 8 }}>
            <View style={styles.row}>
              <View style={{ flex: 1 }}>
                <Text style={{ color: "red" }}>{item.status_name}</Text>
              </View>
              <View style={{ flex: 1 }}>
                <Text style={{ color: "red" }}>{item.created_at}</Text>
              </View>
            </View>
            <View style={[styles.row, { paddingBottom: 16 }]}>
              <Text>
                {item.note}
              </Text>
            </View>
          </View>
        </View>
      ))
    )
  }

  render() {
    const { isLoading, address } = this.state;

    return (
      <ScrollView style={[styles.container]}>
        <View style={{ flex: 1 }}>
          {
            isLoading &&
            <View style={{ flex: 1, justifyContent: "center" }}>
              <Loading />
            </View>
            ||
            <Fragment>
              <View style={{ height: 250, flex: 1, marginBottom: 16 }}>
                <GoogleMap address={address} origin={this.state.location.origin} destination={this.state.location.destination} />
              </View>
              <View style={{ marginBottom: 64, flex: 1, marginHorizontal: 16 }}>
                {this._renderItem()}
              </View>
            </Fragment>
          }
        </View>
      </ScrollView>
    )
  }
}

function mapStateToProps(state) {
  const { accessToken } = state.authReducer
  return { accessToken }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch)
}

const HistoryTreeContainer = connect(mapStateToProps, mapDispatchToProps)(HistoryTreeScreen);
export { HistoryTreeContainer };

