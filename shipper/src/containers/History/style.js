import { colors } from 'config/colors';
import { StyleSheet } from 'react-native';
import { getWidthPercentage } from 'utils';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.bgBody
  },
  containerItem: {
    flex: 1,
    marginVertical: 8,
    backgroundColor: "#fff",
    paddingHorizontal: 16,
    paddingVertical: 8
  },

  row: {
    flex: 1,
    flexDirection: "row"
  },

  lineBottom: {
    paddingBottom: 8,
    borderBottomColor: "#F2F2F2",
    borderBottomWidth: 1
  },

  lineTop: {
    paddingTop: 8,
    borderTopColor: "#F2F2F2",
    borderTopWidth: 1
  },

  containerForm: {
    paddingHorizontal: 16,
  },

  containerImageStep2: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 16
  },

  coverImageStep2: {
    overflow: "hidden",
    height: getWidthPercentage(80),
    width: getWidthPercentage(80),
    borderRadius: getWidthPercentage(80) / 8,
    borderStyle: "dashed",
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
    borderWidth: 1
  },

  titleBlock: {
    paddingVertical: 16,
    fontSize: 18,
    fontWeight: "bold"
  },

  input: {
    backgroundColor: "#fff",
    borderRadius: 8,
    padding: 8,
    marginBottom: 8
  },
  picker: {
    backgroundColor: "#fff",
    borderRadius: 8,
    overflow: 'hidden'
  },
  btn: {
    padding: 16,
    color: "#fff",
    backgroundColor: '#FCB300',
    textAlign: "center",
    fontSize: 16
  },
  btnText: {
    textAlign: "center",
    color: "#fff",
    fontSize: 14
  },
  col: {
    flex: 1,
    marginHorizontal: 8
  },
  label: {
    padding: 2
  },
  containerInput: {
    marginBottom: 8
  },
  line: {
    paddingBottom: 8,
    marginBottom: 8,
    borderColor: "#f0f0f0",
    borderBottomWidth: 1
  },
  buttonFLoatingContainer: {
    position: 'absolute',
    flex: 1,
    width: 60,
    height: 60,
    alignItems: 'center',
    justifyContent: 'center',
    right: 30,
    bottom: 30,
    backgroundColor: 'green',
    elevation: 10,
    borderRadius: 30
  }
})
