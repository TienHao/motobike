// libary
import { addUserInfoAction } from 'actions';
import { callApi, getApiLogin, getDeviceId } from 'api';
import React, { Component } from 'react';
import { StatusBar, Text, TouchableOpacity, TouchableWithoutFeedback, View, ToastAndroid } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { LoginFacebookComp } from './components/LoginFacebook';
import { LoginGoogleComp } from './components/LoginGoogle';
import { styles } from './style';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import firebase from 'react-native-firebase';
import DeviceInfo from 'react-native-device-info';

// rule
import { myRule } from '../../utilities';

//components
import LoginTextInput from './components/LoginTextInput';

class LoginScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: {
        value: "",
        isValidating: null,
        errorMessage: "",
        successMessage: ""
      },
      password: {
        value: "",
        isValidating: null,
        errorMessage: ""
      },
      errorMessage: "",
      token: ''
    }
    this.initialize();
  }

  componentDidMount() {
    this._navListenerFocus = this.props.navigation.addListener('willFocus', () => {
      StatusBar.setHidden(true)
    });

    this._navListenerBlur = this.props.navigation.addListener('willBlur', () => {
      StatusBar.setHidden(false)
    });

    this.onTokenRefreshListener = firebase.messaging().onTokenRefresh(fcmToken => {
      // Process your token as required
    });
  }

  componentWillUnmount() {
    this.onTokenRefreshListener();
    this._navListenerFocus.remove();
    this._navListenerBlur.remove();
  }

  initialize = () => {
    StatusBar.setBarStyle('light-content');
  }

  handleGetToken = async (userInfo) => {
    const authorization = this.props.accessToken;

    firebase
      .messaging()
      .hasPermission()
      .then(enabled => {
        if (enabled) {
          console.log('firebase user has permission', enabled);

          firebase
            .messaging()
            .getToken()
            .then(async function (token) {
              console.log('firebase token has permision:', token);
              console.log('Device Info:', DeviceInfo.getUniqueId());

              const device_id = DeviceInfo.getUniqueId();

              const resInfo = await callApi(getDeviceId({
                authorization, data: {
                  "member_id": userInfo.id,
                  "device_id": device_id,
                  'token': token,
                  'type_id': userInfo.type_id
                }
              }));

              console.log('resInfo', resInfo);
            });
        } else {
          firebase
            .messaging()
            .requestPermission()
            .then(() => {
              firebase
                .messaging()
                .getToken()
                .then(async function (token) {
                  const fcmToken = await firebase.messaging().getToken();
                  console.log('fcmToken', fcmToken);
                  const device_id = DeviceInfo.getUniqueId();

                  const resInfo = await callApi(getDeviceId({
                    authorization, data: {
                      "member_id": userInfo.id,
                      "device_id": device_id,
                      'token': token,
                      'type_id': userInfo.type_id
                    }
                  }));
                });

              console.log('resInfo', resInfo);

              firebase.messaging().onMessage(function (payload) {
                firebase.notifications().displayNotification(notification)
                console.log('Message received');
              });
            })
            .catch(error => {
              console.log('firebase message request permission error:', error);
            });
        }
      });
  }

  onPressLogin = async () => {
    try {
      const authorization = this.props.accessToken;
      const userInfo = this.props.userInfo;
      const { email, password } = this.state;

      let data = { email: email.value, password: password.value };

      let deviceId = DeviceInfo.getUniqueId();

      if (data.email !== '' && data.password !== '') {
        const res = await callApi(getApiLogin({ authorization, data }));

        if (res && res.success) {
          this.setState({ errorMessage: "" });
          this.props.addUserInfoAction({ userInfo: res.data.user_info });
          ToastAndroid.show('Bạn đã đăng nhập thành công! ', ToastAndroid.SHORT);
          await this.handleGetToken(res.data.user_info);
          this.props.navigation.popToTop();
        } else {
          if (res.message.password) {
            this.setState({
              password: {
                isValidating: false,
                errorMessage: res.message.password
              }
            })
          } else if (res.message.error_message) {
            this.setState({
              errorMessage: res.message.error_message + " email hoặc password chưa hợp lệ"
            })
          }
        }
      } else {
        this.setState({
          email: {
            ...this.state.email,
            isValidating: false
          },
          password: {
            ...this.state.password,
            isValidating: false
          }
        });
      }
    } catch (error) {
      // err
    }
  }

  onChangeText = (name, value) => {
    try {
      let checkRuleEmail = myRule.email(value);

      if (name === 'email' && checkRuleEmail == false && value !== '') {
        this.setState({
          [name]: {
            ...this.state[name],
            value: value,
            isValidating: false,
            errorMessage: "Bạn nhập email chưa hợp lệ",
            successMessage: "Bạn nhập email hợp lệ"
          }
        });
      } else {
        this.setState({
          [name]: {
            ...this.state[name],
            value: value,
            isValidating: true
          }
        })
      }
    } catch (error) {

    }
  }

  onEndEditingCheckError = (name, value) => {
    try {
      if (this.state[name].value === '') {
        this.setState({
          [name]: {
            ...this.state[name],
            isValidating: false
          }
        })
      }
    } catch (error) {

    }
  }

  render() {
    const { errorMessage, email, password } = this.state;

    return (
      <View style={styles.container}>
        <View style={{
          right: 0,
          top: 0,
          elevation: 5,
          flexDirection: 'row',
          shadowOpacity: 5,
          padding: 10,
          backgroundColor: 'white'
        }}>
          <TouchableWithoutFeedback
            style={{ width: 50, height: 50 }}
            onPress={() => this.props.navigation.navigate('Home')}
          >
            <MaterialCommunityIcons name='arrow-left' style={{ fontSize: 24 }} />
          </TouchableWithoutFeedback>
          <View style={{ justifyContent: 'center', alignItems: 'center', flex: 6 }}>
            <Text style={{ fontSize: 16 }}>Đăng Nhập</Text>
          </View>
        </View>
        <KeyboardAwareScrollView
          enableOnAndroid={true}
          innerRef={ref => {
            this.scroll = ref
          }}
        >
          <View style={{ marginTop: 20 }}>
            <View style={styles.containerForm}>
              <LoginTextInput
                placeHolder="Nhập tài khoản*"
                value={this.state.email.value}
                name="email"
                errorMessage={this.state.email.errorMessage}
                successMessage={this.state.email.successMessage}
                onEndEditingCheckError={this.onEndEditingCheckError}
                error={email.isValidating !== null ? !email.isValidating : null}
                success={email.isValidating !== null ? email.isValidating : null}
                iconStyleName="account-circle"
                onChangeText={this.onChangeText}
              />

              <LoginTextInput
                placeHolder="Nhập mật khẩu*"
                iconStyleName="key"
                name="password"
                onEndEditingCheckError={this.onEndEditingCheckError}
                style={{ marginTop: 20 }}
                errorMessage={this.state.password.errorMessage}
                value={password.value}
                onChangeText={this.onChangeText}
                error={password.isValidating !== null ? !password.isValidating : null}
                success={password.isValidating !== null ? password.isValidating : null}
              />

              <View style={{ flex: 1, flexDirection: "column" }}>
                <View style={{ marginTop: 5 }}>
                  <Text style={{ color: "red" }}>
                    {errorMessage}
                  </Text>
                </View>
                <View style={{ flex: 1 }}>
                  <TouchableOpacity
                    style={styles.btn}
                    onPress={() => this.onPressLogin()}
                  >
                    <Text style={styles.btnText}>Đăng nhập</Text>
                  </TouchableOpacity>
                </View>

                <View style={{ flex: 1, marginTop: 8, flexDirection: "row", justifyContent: "center" }}>
                  <Text>Bạn chưa có tài khoản ?</Text>
                  <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('Register')}
                  >
                    <Text style={{ textAlign: "center", paddingLeft: 4, color: 'blue', textDecorationLine: 'underline' }}>Đăng Ký ngay</Text>
                  </TouchableOpacity>
                </View>

                <View style={{ flex: 1, marginTop: 8 }}>
                  <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('ResetPass')}
                  >
                    <Text style={{ textAlign: "center", paddingLeft: 4, color: 'blue', textDecorationLine: 'underline' }}>Quên mật khẩu</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>

            <View style={styles.containerSeparate}>
              <View style={{ flex: 2, borderBottomWidth: 1, borderBottomColor: "gray" }}>
              </View>
              <View style={{ flex: 1 }}>
                <Text style={{ textAlign: "center", color: "gray" }}>Hoặc</Text>
              </View>
              <View style={{ flex: 2, borderBottomWidth: 1, borderBottomColor: "gray" }}>
              </View>
            </View>

            <View style={styles.containerLoginSocial}>
              <View style={styles.socialItem}>
                <LoginFacebookComp {...this.props} />
              </View>
              <View style={styles.socialItem}>
                <LoginGoogleComp {...this.props} />
              </View>
            </View>
          </View>
        </KeyboardAwareScrollView>
      </View>
    );
  }
}

function mapStateToProps(state) {
  const { accessToken, userInfo } = state.authReducer;
  return { accessToken, userInfo };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ addUserInfoAction }, dispatch);
}

const LoginContainer = connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
export { LoginContainer };

