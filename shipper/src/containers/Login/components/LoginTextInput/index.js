import React from "react";
import { StyleSheet, View, TextInput, Text } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

function LoginTextInput(props) {
    function onchangeTextInput(value) {
        props.onChangeText(props.name, value);
    }

    function onEndEditingCheckError(value) {
        props.onEndEditingCheckError(props.name, value);
    }

    const { name, value } = props;

    return (

        <View>
            <View style={[styles.container, props.style]}>
                <Icon
                    name={props.iconStyleName}
                    style={styles.iconStyle}
                />

                <TextInput
                    onEndEditing={(e) => onEndEditingCheckError(e.nativeEvent.text)}
                    placeholder={props.placeHolder || "Place Holder"}
                    onChangeText={(text) => onchangeTextInput(text)}
                    name={name}
                    value={value}
                    secureTextEntry={name === 'password'}
                    style={[styles.inputStyle, {
                        borderBottomColor: props.error ? "red" : props.success ? "green" : "#D9D5DC"
                    }]}

                />

            </View>
            {props.error ? (
                <Text
                    style={[
                        styles.errorMessage,
                        {
                            color: props.error ? "red" : "transparent"
                        }
                    ]}
                >
                    {props.errorMessage || "Băt buộc nhập"}
                </Text>
            ) : null}
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: "transparent",
        flexDirection: "row",
        alignItems: "center",
        borderBottomWidth: 1
    },
    inputStyle: {
        height: 42,
        color: "#000",
        alignSelf: "stretch",
        paddingTop: 14,
        paddingRight: 16,
        paddingBottom: 8,
        fontSize: 16,
        fontFamily: "roboto-regular",
        lineHeight: 16
    },
    iconStyle: {
        color: "#616161",
        fontFamily: "Roboto",
        fontSize: 24,
        paddingRight: 8,
        width: 32,
        height: 24
    }
});

export default LoginTextInput;
