// libary
import { GoogleSignin, statusCodes } from '@react-native-community/google-signin';
import { addUserInfoAction } from 'actions';
import { callApi, getApiLoginSocial } from 'api';
import React, { Component, Fragment } from 'react';
import { Text, TouchableHighlight, View, ToastAndroid } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { styles } from '../style';

class LoginGoogle extends Component {
  constructor(props) {
    super(props)

    this.initializeState()
    this.initialize()
  }

  initializeState = () => {
    this.state = {
      isLoginGoogle: false,
      userInfo: {}
    }
  }

  initialize = () => {
    this.checkTokenGoogle()
    this.registerGoogleConfig()
  }

  registerGoogleConfig = () => {
    GoogleSignin.configure({
      webClientId: "573581155022-2ce7nr4j6l9hgkurbtq0mcumrju70541.apps.googleusercontent.com",
      offlineAccess: false,
      forceConsentPrompt: true
    })
  }

  getCurrentUser = async () => {
    const currentUser = await GoogleSignin.getCurrentUser();
    this.setState({ currentUser });
  };

  checkTokenGoogle = async () => {
    const isSignedIn = await GoogleSignin.isSignedIn();
    this.setState({ isLoginGoogle: isSignedIn });
  }

  signInGoogle = async () => {
    try {
      await GoogleSignin.hasPlayServices({ showPlayServicesUpdateDialog: true });

      const { user } = await GoogleSignin.signIn();

      const authorization = this.props.accessToken;

      const res = await callApi(getApiLoginSocial({
        authorization, data: {
          // email: user.email,
          // fullname: user.name
        }
      }));

      this.props.addUserInfoAction({ userInfo: res.user_info });
      // ToastAndroid.show('Bạn đăng nhập thành công!', ToastAndroid.LONG);
      this.props.navigation.popToTop();
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
      } else if (error.code === statusCodes.IN_PROGRESS) {
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
      } else {
      }
    }
  }

  signOutGoogle = async () => {
    try {
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();
      this.setState({ userInfo: null, isLoginGoogle: false });
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    const { isLoginGoogle } = this.state;

    return (
      <Fragment>
        {
          !isLoginGoogle
          &&
          <TouchableHighlight
            style={styles.btnGoogle}
            underlayColor="#FF5040"
            onPress={() => this.signInGoogle()}
          >
            <View style={{ flexDirection: "row" }}>
              <View style={{ flex: 2, alignItems: 'flex-end' }}>
                <Icon name="google" style={{ color: "white", fontSize: 35 }} />
              </View>
              <View style={{ flex: 8, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ textAlign: "center", color: "#fff", paddingLeft: 8 }}>Đăng nhập sử dụng google</Text>
              </View>
            </View>
          </TouchableHighlight>
          ||
          <TouchableHighlight
            style={styles.btnGoogle}
            onPress={() => this.signOutGoogle()}
          >
            <Text style={{ textAlign: "center", color: "#fff", paddingLeft: 8 }}>Sign out</Text>
          </TouchableHighlight>
        }
      </Fragment>
    )
  }
}

function mapStateToProps(state) {
  const { accessToken } = state.authReducer
  return { accessToken }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ addUserInfoAction }, dispatch)
}

const LoginGoogleComp = connect(mapStateToProps, mapDispatchToProps)(LoginGoogle);
export { LoginGoogleComp };

