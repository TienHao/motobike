import { addUserInfoAction } from 'actions';
import { callApi, getApiLoginSocial } from '../../../api';
import React, { Component, Fragment } from 'react';
import { Text, TouchableHighlight, View } from 'react-native';
import { AccessToken, GraphRequest, GraphRequestManager, LoginManager } from 'react-native-fbsdk';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { styles } from '../style';

class LoginFacebook extends Component {
  constructor(props) {
    super(props)

    this.initializeState();
  }

  initializeState = () => {
    this.state = {
      isLoginFacebook: false,
      userInfo: {}
    }
  }

  initialize = () => {
    this.checkTokenFacebook();
  }

  checkTokenFacebook = () => {
    AccessToken.getCurrentAccessToken().then(token => {
      if (token) {
        this.setState({ isLoginFacebook: true });
      } else {
        this.setState({ isLoginFacebook: false });
      }
    })
  }

  responseInfoCallback = async (error, result) => {
    if (error) {
      console.log('Error fetching data: ', error);
    } else {
      const authorization = this.props.accessToken;

      console.log('result', result);

      const res = await callApi(getApiLoginSocial({
        authorization, data: {
          // email: result.email,
          // fullname: result.name,
          // phone: result.phone,
          // address: ''
        }
      }));

      this.props.addUserInfoAction({ userInfo: res.user_info });
      this.props.navigation.popToTop();
    }
  }

  signOutFacebook = async () => {
    await LoginManager.logOut();
    this.setState({ isLoginFacebook: false });
  }

  signInFacebook = async () => {
    try {
      const that = this;

      await LoginManager.logInWithPermissions(["public_profile", "email"]).then(
        function (result) {
          if (result.isCancelled) {
            console.log("Login cancelled");
          } else {
            const infoRequest = new GraphRequest(
              '/me?fields=email,name,first_name,last_name',
              null,
              that.responseInfoCallback
            );
            new GraphRequestManager().addRequest(infoRequest).start();
          }
        }
      )

    } catch (error) {
      console.log("Login fail with error: ", { error });
    }
  }

  finishedSignIn = () => {
    this.props.navigation.navigate('Home');
  }

  render() {
    return (
      <Fragment>
        <TouchableHighlight
          style={styles.btnFacebook}
          underlayColor="#4F75C9"
          onPress={() => this.signInFacebook()}
        >

          <View style={{ flex: 1, flexDirection: "row" }}>
            <View style={{ flex: 2, alignItems: 'flex-end' }}>
              <Icon name="facebook" style={{ color: "white", fontSize: 35 }} />
            </View>
            <View style={{ flex: 8, justifyContent: 'center', alignItems: 'center' }}>
              <Text style={{ textAlign: "center", color: "#fff", paddingLeft: 8 }}>Đăng nhập sử dụng facebook</Text>
            </View>
          </View>
        </TouchableHighlight>
      </Fragment>
    )
  }
}

function mapStateToProps(state) {
  const { accessToken } = state.authReducer;
  return { accessToken }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ addUserInfoAction }, dispatch);
}

const LoginFacebookComp = connect(mapStateToProps, mapDispatchToProps)(LoginFacebook);
export { LoginFacebookComp };

