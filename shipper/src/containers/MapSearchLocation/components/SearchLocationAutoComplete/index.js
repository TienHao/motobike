// libary
import React, { Component, Fragment } from 'react';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome5';
import { View, TouchableOpacity, Text, } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { connect } from 'react-redux';

// actions
import { getAddressDelivery } from '../../../../actions';

class SearchLocationAutoComplete extends Component {
    constructor(props) {
        super(props);
    }

    onpressPostAddress = (address, data) => {
        if (this.props.valueCheckPeople === 1) {
            this.props.getAddressDelivery({ addressDeliver: address, location: data.geometry.location });
            this.props.navigation.navigate("Order");
        } else if (this.props.valueCheckPeople === 2) {
            this.props.getAddressDelivery({ addressReceiver: address, location: data.geometry.location });
            this.props.navigation.navigate("Order");
        }
    }

    render() {
        return (
            <Fragment>
                <View style={{
                    borderColor: 'grey',
                    borderWidth: 1,
                    padding: 8,
                    flexDirection: 'row',
                    marginTop: 10,
                    marginLeft: 5,
                    marginRight: 5
                }}>
                    <View style={{ justifyContent: 'center', flex: 1, alignItems: 'center' }}>
                        <MaterialIcons name="edit-location" style={{ fontSize: 24, color: 'grey' }} />
                    </View>
                    <View style={{ flex: 9, justifyContent: 'center' }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate("MapGetLocation")}>
                            <Text>Chọn vị trí</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <GooglePlacesAutocomplete
                    debounce={600}
                    minLength={3}
                    placeholder="Tìm kiếm vị trí"
                    onPress={(data, details) => this.onpressPostAddress(data.description, details)}
                    renderLeftButton={() =>
                        <View style={{ justifyContent: 'center', alignItems: 'center', marginLeft: 10 }}>
                            <IconFontAwesome name="search-location" style={{ fontSize: 24, color: 'grey' }} />
                        </View>}
                    renderDescription={row => row.description}
                    listViewDisplayed='auto'
                    returnKeyType={'default'}
                    query={{
                        key: "AIzaSyCx3c8GVP57CDXvA8l1iCNRlMFrXdj7G_0",
                        language: 'vi'
                    }}
                    styles={{
                        textInputContainer: {
                            backgroundColor: 'white',
                            margin: 5,
                            borderTopWidth: 1,
                            borderBottomWidth: 1,
                            borderWidth: 1,
                            borderColor: 'grey'
                        },
                        textInput: {
                            marginLeft: 5,
                            marginRight: 5,
                            color: '#5d5d5d',
                            fontSize: 16,

                        },
                        predefinedPlacesDescription: {
                            color: '#1faadb'
                        },
                    }}
                    autoFocus={false}
                    filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']}
                    enablePowerByContainer={false}
                    fetchDetails={true}
                    currentLocation={false}
                    predefinedPlacesAlwaysVisible={true}
                />
            </Fragment>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        valueCheckPeople: state.mapReducer.valueCheckPeople
    }
}

const mapDispatchToProps = {
    getAddressDelivery
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchLocationAutoComplete);