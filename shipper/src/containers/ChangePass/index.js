import { callApi, getApiChangePass } from 'api';
import { Toast } from 'components/Toast';
import { MyInputText } from 'controls';
import React, { PureComponent } from 'react';
import { findNodeHandle, ScrollView, Text, TouchableOpacity, View } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { myRule } from 'utils';
import { styles } from './style';

class ChangePassScreen extends PureComponent {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Đổi mật khẩu'
    };
  };

  constructor(props) {
    super(props)

    this._validForm = []
    this._positionControls = []

    this.state = this._getInitialState()
  }

  _getInitialState = () => {
    return {
      isCheckValid: false,
      password: "",
      newPassword: "",
      confirmNewPassword: "",
      toastType: "",
      isActiveToast: false,
      messageToast: ""
    }
  }

  _resetState = () => {
    this.setState({
      password: "",
      newPassword: "",
      confirmNewPassword: ""
    })
  }

  _handleChange = ({ name, value }) => {
    this.setState({ [name]: value, isActiveToast: false})
  }

  _scrollTo = ({x, y}) => {
    this._scrollView.scrollTo({x: 0, y, animated: true})
  }

  _calculatePosition = (name) => {
    this._positionControls[name].measureLayout(
      findNodeHandle(this._scrollView),
      (x,y) => {
        this._scrollTo({x, y})
      }
    )
  }

  _checkValidate = async () => {
    try {
      this._validForm = []
      await this._setStateAsync({ isCheckValid: true })
      this.forceUpdate()

      return {
        valid: this._validForm.every(control => {
          if(!control.valid) {
            this._calculatePosition(control.name)
            return false
          }
          return true
        }),
        data: {
          email: this.props.userInfo.email,
          password: this.state.password,
          new_password: this.state.newPassword,
          re_new_password: this.state.confirmNewPassword
        }
      }

    } catch(error) {
      console.log('ChangePass', { error })
      return false
    }
  }

  _changePass = async () => {
    const authorization = this.props.accessToken
    const { valid, data } = await this._checkValidate()

    if(valid) {
      const res = await callApi(getApiChangePass({ authorization, data  }))

      if(res && res.success) {
        this.setState({ messageToast: "Cập nhật thành công", isActiveToast: true, toastType: "success"}, () =>
          setTimeout(() => {
            this.props.navigation.pop()
          }, 2000)
        )
      } else {
        this.setState({ messageToast: res.message.error_message, isActiveToast: true, toastType: "error"})
      }
    }
  }

  render() {
    const { isCheckValid } = this.state
    return (
      <>
        <Toast
          type={this.state.toastType}
          active={this.state.isActiveToast}
          message={this.state.messageToast}
        />
        <ScrollView
          style={styles.container}
          ref={ el => this._scrollView = el }
        >
          <View
            ref={el => this._positionControls["password"] = el }
          >
            <MyInputText
              type="password"
              name={"password"}
              label={"Mật khẩu hiện tại"}
              placeholder={"Nhập mật khẩu hiện tại"}
              value={this.state.password}
              rules={[myRule.required, myRule.minLength.bind(null, [8])]}
              checkValidForm={isCheckValid}
              onChangeText={this._handleChange}
              onValidate={(control) => this._validForm.push(control)}
            />
          </View>

          <View
            ref={el => this._positionControls["newPassword"] = el }
          >
            <MyInputText
              type="password"
              name={"newPassword"}
              label={"Mật khẩu mới"}
              placeholder={"Nhập mật khẩu mới"}
              value={this.state.newPassword}
              rules={[myRule.required, myRule.minLength.bind(null, [8])]}
              checkValidForm={isCheckValid}
              onChangeText={this._handleChange}
              onValidate={(control) => this._validForm.push(control)}
            />
          </View>

          <View
            ref={el => this._positionControls["confirmNewPassword"] = el }
          >
            <MyInputText
              type="password"
              name={"confirmNewPassword"}
              label={"Mật lại mật khẩu mới"}
              placeholder={"Nhập lại mật khẩu mới"}
              value={this.state.confirmNewPassword}

              rules={[ myRule.required, myRule.equal.bind(null, [this.state.newPassword, 'Mật khẩu không trùng khớp']) ]}
              checkValidForm={isCheckValid}
              onChangeText={this._handleChange}
              onValidate={(control) => this._validForm.push(control)}
            />
          </View>

          <View
            style={styles.btn}
          >
            <TouchableOpacity
              onPress={() => this._changePass()}
            >
              <Text style={{textAlign: "center", color: "#fff" }}>Đổi mật khẩu</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </>
    )
  }
}


function mapStateToProps(state) {
  const {accessToken, userInfo} = state.authReducer
  return {accessToken, userInfo}
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch)
}

const ChangePassContainer = connect(mapStateToProps, mapDispatchToProps)(ChangePassScreen);
export { ChangePassContainer };

