// libary
import React, { Component } from 'react';
import { View, AsyncStorage, TouchableOpacity, Text, Alert } from 'react-native';
import firebase from 'react-native-firebase';

class OfoNotification extends Component {
    constructor(props) {
        super(props);
    }

    async componentDidMount() {
        this.onTokenRefreshListener = await firebase.messaging().onTokenRefresh(fcmToken => {
            // Process your token as required
        });

        this.checkPermision();
        this.createNotificationListeners();
    }



    checkPermision = async () => {
        const hasPermision = await firebase.messaging().hasPermission();

        if (hasPermision) {
            this.getToken();
        } else {
            this.requestPermission();
        }
    }

    async requestPermission() {
        try {
            await firebase.messaging().requestPermission();

            this.getToken();
        } catch (error) {
            console.log('Bạn không có quyền từ điện thoại của bạn!');
        }
    }

    async createNotificationListeners() {
        try {
            this.notificationListener = firebase.notifications().onNotification((noti) => {
                const localNotification = new firebase.notifications.Notification({
                    show_in_foreground: true,
                })
                    .setNotificationId(noti._notificationId)
                    .setTitle(noti.title)
                    .setBody(noti.body)
                    .android.setChannelId('fcm_FirebaseNotifiction_default_channel')
                    .android.setColor('#000000')
                    .android.setPriority(firebase.notifications.Android.Priority.High);

                console.log('localNotification', localNotification);

                firebase.notifications()
                    .displayNotification(localNotification)
                    .catch(err => console.log("errornoti", err));
            });

            const channel = new firebase.notifications.Android.Channel('fcm_FirebaseNotifiction_default_channel', "Demo app name", firebase.notifications.Android.Priority.High)
                .setDescription('Demo app description')

            firebase.notifications().android.createChannel(channel);

            this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
                const { title, body } = notificationOpen;

                this.openAlert(title, body);
            })

            const notificationOpen = await firebase.notifications().getInitialNotification();

            if (notificationOpen) {
                const { title, body } = notificationOpen.notification;

                this.openAlert(title, body);
            }

            this.messageListener = firebase.messaging().onMessage((message) => {
                console.log('error', JSON.stringify(message));
            })
        } catch (error) {
            console.log('error', error);
        }
    }

    openAlert = (title, body) => {
        console.log('title', title);
        console.log('body', body);
        Alert.alert(title, body);
    }

    async getToken() {
        let firebaseCloudToken = await AsyncStorage.getItem('token1');
        console.log('token', firebaseCloudToken);
        if (!firebaseCloudToken) {
            firebaseCloudToken = await firebase.messaging().getToken().then(fcmToken => {
                console.log('token', fcmToken);
            });

            if (firebaseCloudToken) {
                await AsyncStorage.setItem('token1', firebaseCloudToken);
            }
        }
    }

    componentWillUnmount() {
        this.notificationListener();
        this.notificationOpenedListener();
        this.onTokenRefreshListener();
    }

    render() {
        return (
            <View>
                <TouchableOpacity onPress={() => this.notificationOpenedListener()}>
                    <Text>Hiển thị Thông báo</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

export default OfoNotification;