import React, { Component } from 'react';
import { View, Text } from 'react-native';

class HistoryNotification extends Component {
    constructor(props) {
        super(props);
    }

    static navigationOptions = ({ navigation }) => {
        return {
            title: "Thông báo"
        }
    };

    render() {
        return (
            <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
                <Text>
                    {"Hiện tại chưa có thông báo nào!"}
                </Text>
            </View>
        );
    }
}

export default HistoryNotification;