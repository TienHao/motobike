export const GET_ACCESS_TOKEN = "GET_ACCESS_TOKEN";
export const GET_ACCESS_TOKEN_SUCCESS = "GET_ACCESS_TOKEN_SUCCESS";
export const GET_ACCESS_TOKEN_FAIL = "GET_ACCESS_TOKEN_FAIL";

export const GET_USER_INFO = "GET_USER_INFO";
export const GET_USER_INFO_SUCCESS = "GET_USER_INFO_SUCCESS";
export const GET_USER_INFO_FAIL = "GET_USER_INFO_FAIL";

export const ADD_USER_INFO = "ADD_USER_INFO";
export const ADD_USER_INFO_SUCCESS = "ADD_USER_INFO_SUCCESS";
export const ADD_USER_INFO_FAIL = "ADD_USER_INFO_FAIL";

export const REMOVE_USER_INFO = "REMOVE_USER_INFO";
export const REMOVE_USER_INFO_SUCCESS = "REMOVE_USER_INFO_SUCCESS";
export const REMOVE_USER_INFO_FAIL = "REMOVE_USER_INFO_FAIL";

// for bindActionCreators in containers
export const getAccessTokenAction = function() {
  return {
    type: GET_ACCESS_TOKEN,
    payload: {}
  }
}

export const getUserInfoAction = function() {
  return {
    type: GET_USER_INFO,
    payload: {}
  }
}

export const addUserInfoAction = function({ userInfo }) {
  return {
    type: ADD_USER_INFO,
    payload: { data: userInfo}
  }
}

export const removeUserInfoAction = function() {
  return {
    type: REMOVE_USER_INFO,
    payload: {}
  }
}
