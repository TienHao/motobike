export const changeStatusPushNotification = function (payload, type) {
    return {
        type: type,
        payload: payload
    }
}