export const currencyFormat1 = function (num) {
  try {
    const format = num && Number(num).toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') || ""
    return format;

  } catch (error) {
    console.log(`currencyFormat: ${num}`, error);
    return false
  }
}

function checkDecimal(number) {
  return (number - Math.floor(number) !== 0);
}

export const currencyFormat = function (num) {
  let check = checkDecimal(num);
  let format = null;
  try {
    if (check) {
      format = num + '00';
    } else {
      format = num + '000';
    }
    return format

  } catch (error) {
    console.log(`currencyFormat: ${num}`, error)
    return false
  }
}

export const removeComma = function (num) {
  try {
    const format = (num && parseInt(num.replace(/,/g, '')) || "");
    return format;

  } catch (error) {
    return false
  }
}


