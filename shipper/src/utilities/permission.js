import Permissions from 'react-native-permissions';

export const permissionType = {
  photo: "photo",
  camera: "camera",
  location: "location"
}

const permissionValue = {
  authorized: "authorized",
  denied: "denied",
  restricted: "restricted",
  undetermined: "undetermined"
}

export const takePermission = async function(permissionType) {
  return await Permissions.check(permissionType, {type: 'always'}).then(async (permission) => {
    if(permission === permissionValue.authorized) {
      return true
    }

    if(permission === permissionValue.undetermined) {
      const res = await Permissions.request(permissionType)
      if(res === permissionValue.authorized) {
        return true
      }

      if(res === permissionValue.denied || res === permissionValue.restricted) {
        return false
      }
    }

    if(permission === permissionValue.denied) {
      const res = await Permissions.request(permissionType)
      if(res === permissionValue.authorized) {
        return true
      }

      if(res === permissionValue.denied || res === permissionValue.restricted) {
        return false
      }
    }

    if(permission === permissionValue.restricted) {
      const res = await Permissions.request(permissionType)
      if(res === permissionValue.denied || res === permissionValue.restricted) {
        return false
      }
    }

    return false
  })
}
