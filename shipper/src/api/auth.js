import { baseURL, defaultSecret } from "./services";

function mapProperty(api, data) {
  switch (api) {
    case "register": {
      const { fullName, email, phone, password, confirm, type_id } = data;

      return {
        fullName,
        email,
        phone,
        password,
        confirm,
        type_id: type_id
      }
    }
    case "login": {
      return { email: data.email || "", password: data.password || "" };
    }
  }
}

export const getApiAccessToken = function () {
  return {
    method: 'POST',
    url: baseURL + '/auth',
    body: defaultSecret
  }
}

export const getApiRegister = ({ data }) => {
  const mapData = mapProperty("register", data);

  return {
    method: 'POST',
    url: baseURL + '/auth/register',
    body: mapData
  }
}

export const getApiLoginSocial = ({ data, authorization }) => {
  data.type_id = 0;

  return {
    method: 'POST',
    url: baseURL + '/auth/register_fb',
    body: data,
    authorization
  }
}

export const getApiLogin = ({ data, authorization }) => {
  const mapData = mapProperty("login", data);

  return {
    method: 'POST',
    url: baseURL + '/member/login',
    body: mapData,
    authorization
  }
}

export const getApiUpdateUserInfo = ({ authorization, data }) => {
  return {
    method: 'POST',
    url: baseURL + '/member/update',
    body: data,
    authorization
  }
}

export const getApiChangePass = ({ authorization, data }) => {
  return {
    method: 'POST',
    url: baseURL + '/member/change_pass',
    body: data,
    authorization
  }
}

export const getApiResetPass = ({ authorization, data }) => {
  return {
    method: 'POST',
    url: baseURL + '/member/reset_pass',
    body: data,
    authorization
  }
}
