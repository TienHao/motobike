import { baseURL } from "./services";

export const getApiDistricts = ({ authorization, provinceId }) => {
  return {
    method: 'GET',
    url: baseURL + `/address/districts?province_id=${provinceId}`,
    authorization
  }
}

export const getApiWards = ({ authorization, districtId }) => {
  return {
    method: 'GET',
    url: baseURL + `/address/wards?district_id=${districtId}`,
    authorization
  }
}
