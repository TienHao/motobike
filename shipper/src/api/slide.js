import { baseURL } from "./services";

export const getApiSlides = ({ authorization }) => {
  return {
    method: 'GET',
    url: baseURL  + '/slide?id=12', // current id for slide,
    authorization
  }
}
