// libary
import { createAppContainer, createSwitchNavigator } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import { SplashContainer } from '../containers/Splash/index';

import {
  ArticleStack,
  ChangePassStack,
  MapGetLocationStack,
  ContactStack,
  OrderInProgressStack,
  ErrorStack,
  HistoryStack,
  HistoryTreeStack,
  MapStack,
  OrderStack,
  ResetPassStack,
  UserInfoStack,
  NotificationStack
} from './stack-navigator';

import { AuthSwitch } from './switch-navigator';
import { TabBottomNavigator } from './TabBottomNavigator';

const MainApp = createStackNavigator(
  {
    TabBottomNavigator: TabBottomNavigator,
    Auth: AuthSwitch,
    Order: OrderStack,
    OrderInProgress: OrderInProgressStack,
    Contact: ContactStack,
    Map: MapStack,
    Article: ArticleStack,
    Error: ErrorStack,
    UserInfo: UserInfoStack,
    History: HistoryStack,
    HistoryTree: HistoryTreeStack,
    ChangePass: ChangePassStack,
    ResetPass: ResetPassStack,
    MapGetLocation: MapGetLocationStack,
    Notification: NotificationStack
  },
  {
    initialRouteName: 'TabBottomNavigator',
    headerMode: "none"
  }
)

const AppNavigator = createSwitchNavigator(
  {
    Splash: {
      screen: SplashContainer,
      navigationOptions: ({ navigation }) => ({
        header: () => null
      })
    },
    Main: MainApp
  },
  {
    initialRouteName: 'Splash',
  }
);

const AppContainer = createAppContainer(AppNavigator);

export {
  AppContainer
}
