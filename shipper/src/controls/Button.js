import React, { PureComponent } from 'react';
import { StyleSheet, Text, TouchableWithoutFeedback, View } from 'react-native';

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    alignItems: "center",
    height: 50,
    borderRadius: 10,
    borderColor: "pink",
    color:"pink"
  },

  buttonFocus: {
    textAlign: "center",
    color: "#fff"
  },

  buttonBlur: {

    textAlign: "center",
    color: "pink"
  }
})

class Button extends PureComponent {
  constructor(props) {
    super(props)

    this.state = this._getInitialState()
  }

  _getInitialState = () => {
    return {
      isButtonFocus: false
    }
  }

  _onPress = () => {
    console.log('hihi')
    // const { isButtonFocus } = this.state
    // this.setState({isButtonFocus: !isButtonFocus})
  }

  _onPressIn = () => {
    this.setState({isButtonFocus: true})
    console.log('in')
  }

  _onPressOut = () => {
    console.log('out')
    this.setState({isButtonFocus: false})
  }

  _effect = () => {
    const { isButtonFocus } = this.state
    return {
      backgroundColor: isButtonFocus ? "pink" : "transparent",
      borderWidth: isButtonFocus ? 0 : 3
    }
  }

  _effectText = () => {
    const { isButtonFocus } = this.state
    return {
      color: isButtonFocus ? "#fff" : "pink"
    }
  }

  render() {
    const { title } = this.props
    const { isButtonFocus } = this.state
    return(
      <TouchableWithoutFeedback
        onPressIn={this._onPressIn}
        onPressOut={this._onPressOut}
        onPress={this._onPress}
      >
        <View
          style={{...styles.container, ...this._effect()}}
        >
          <Text style={{...this._effectText()}}>{title}</Text>
        </View>
      </TouchableWithoutFeedback>
    )
  }
}

export { Button };

