import React, { PureComponent } from 'react';
import { StyleSheet, Text, TextInput, View } from 'react-native';
import { currencyFormat, removeComma } from 'utils';

const styles = StyleSheet.create({
  container: {
    flex: 1
  },

  containerLabel: {
    flexDirection: "row",
    alignItems: "center",
  },
  containerError: {
    height: 16,
    paddingLeft: 4
  },
  labelError: {
    color: "red",
    fontSize: 13
  },
  label: {
    padding: 2,
    fontSize: 12,
    color: 'grey',
    paddingRight: 14
  },

  input: {
    flex: 1,
    borderBottomWidth: 1,
    borderColor: 'grey',
    overflow: 'hidden'
  },

  error: {
    borderColor: "red",
    borderBottomWidth: 1,
  },

  hidden: {
    height: 0,
    width: 0
  }
})

class MyInputText extends PureComponent {
  constructor(props) {
    super(props)

    this.state = {
      isFocus: false,
      errorMessage: ""
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { checkValidForm, rules, name, value, editable } = nextProps

    if (checkValidForm && editable) {
      const errorMessage = this._validateByRule(rules, name, value)
      this.setState({ errorMessage })
    }
  }

  componentDidMount() {
    const { rules, name, value, editable } = this.props
    if (editable) this._validateByRule(rules, name, value)
  }

  _onBlur = () => {
    const { rules, name, value, editable } = this.props
    if (editable) {
      const errorMessage = this._validateByRule(rules, name, value)
      this.setState({ errorMessage })
    }
  }

  _handleChangeText = (text) => {
    const { name, useFormatCurrency } = this.props
    this.props.onChangeText({ name, value: (useFormatCurrency && removeComma(text) || text) })
  }

  _validateByRule = (rules, name, value) => {
    if (rules) {
      for (let rule of rules) {
        const message = rule(value)
        if (typeof message === "string") {
          this.props.onValidate({
            name,
            valid: false
          })
          return message
        }
      }

      this.props.onValidate({
        name,
        valid: true
      })
      return null
    }

    return null
  }

  _renderLabel = () => {
    const { label } = this.props;
    if (!label) return null;

    return (
      <View style={[styles.containerLabel]}>
        <Text style={styles.label}> {label} </Text>
      </View>
    )
  }

  _renderErrorMessage = () => {
    const { error } = this.props
    const { errorMessage } = this.state

    return (
      <View style={styles.containerError}>
        <Text style={styles.labelError}>
          {errorMessage || error}
        </Text>
      </View>
    )
  }

  _renderInput = () => {
    const {
      name,
      value,
      keyboardType,
      placeholder,
      type,
      useFormatCurrency,
      editable,
      weightValidate,
      disabled } = this.props;
    const { errorMessage } = this.state;

    return (
      <View pointerEvents={disabled ? "none" : "auto"}
        style={[styles.input, (errorMessage || weightValidate ? styles.error : null)]}>
        <TextInput
          name={name}
          maxLength={this.props.weights == true ? 2 : 10000000}
          onChangeText={(text) => this._handleChangeText(text)}
          onBlur={() => this._onBlur()}
          value={useFormatCurrency && currencyFormat(value) || value}
          keyboardType={keyboardType}
          placeholder={placeholder}
          secureTextEntry={(type === "password")}
          editable={editable}
        />
      </View>
    )
  }

  render() {
    const { style } = this.props
    return (
      <View
        style={[styles.container, style]}
      >
        {this._renderLabel()}
        {this._renderInput()}
        {this._renderErrorMessage()}
      </View>
    )
  }
}

MyInputText.defaultProps = {
  label: "",
  name: "",
  value: "",
  placeholder: "",
  type: "",
  error: "",
  keyboardType: "default",
  useFormatCurrency: false,
  editable: true,
  checkValidForm: false,
  disabled: false
}

export { MyInputText };

