export const colors = {
  bgBody: "#F7F7F7",

  yellow: {
    default: '#FCB300'
  },

  primary: {
    default: "#FFA910",
    light: "#FF0001",
    dark: "#A30001",
    accent: "#D02239"
  },

  secondary: {
    default: "#FFFC07",
    light: "",
    dark: "#FFE70A",
    accent: "#FFF918"
  },

  light: {
    default: "",
    light: "",
    dark: "",
    accent: ""
  },

  dark: {
    default: "",
    light: "",
    dark: "",
    accent: ""
  },

  accent: {
    default: "#FFBF01",
    light: "",
    dark: "",
    accent: ""
  }
}
