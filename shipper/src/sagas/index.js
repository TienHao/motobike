import * as actions from 'actions/index';
import { all, fork, takeEvery } from 'redux-saga/effects';
import authSagas from './auth';

const watchAsync = () => (
  function* watch() {
    yield takeEvery(actions.GET_ACCESS_TOKEN, authSagas.getAccessToken)
    yield takeEvery(actions.GET_USER_INFO, authSagas.getUserInfo)
    yield takeEvery(actions.ADD_USER_INFO, authSagas.addUserInfo)
    yield takeEvery(actions.REMOVE_USER_INFO, authSagas.removeUserInfo)
    // yield takeEvery(actions.GET_SLIDE, slideSagas.getSlide)
  }
)


export default function* rootSaga() {
  yield all([
    fork(watchAsync())
  ])
}
